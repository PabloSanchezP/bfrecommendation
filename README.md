# BFRecommendation

Framework to replicate the experiments published in:

*Revisiting Neighbourhood-Based Recommenders For Temporal Scenarios* in Temporal Reasoning in Recommender Systems Workshop (RecSys2017)

*Time and Sequence Awareness in Similarity Metrics for Recommendation*, Information Processing & Management, Volume 57, Issue 3, May 2020

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for replication

### Prerequisites

Java 8

Maven

Mono

## Dependencies
[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)

Rank fusion library, provided as an independent JAR.


## Instructions
* Steps to install the library and download all the data and scripts:
  * git clone https://bitbucket.org/PabloSanchezP/bfrecommendation
  * cd bfrecommendation
  * Depending on the experiments that you want to replicate, switch between branches.
    * RecTemp2017 for *Revisiting Neighbourhood-Based Recommenders For Temporal Scenarios*
    * master for *Time and Sequence Awareness in Similarity Metrics for Recommendation*
  * For RecTemp2017, check the README.txt in that branch
  * For the ACMTOIS, execute the scripts in the following order:
    * step1_preparedata.sh
    * step2_save_similarities.sh
    * step3_generate_recommenders.sh
    * step4_evaluation.sh
    * step5_post_evaluation.sh



## Authors

* **Pablo Sanchez** - [Universidad Autonoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
* **Alejandro Bellogin** - [Universidad Autonoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)


## License

This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

* This work was funded by the research project TIN2016-80630-P (MINECO)
