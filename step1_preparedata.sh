#!/bin/sh

: '
  First script. It splits the datasets in the two temporal splits.
  It also downloads MyMediaLite framework and compile our version of Ruining He recommenders
'

# Global variables
JAR=LCSBackwardForwardRecommenders/target/LCSBackwardForwardRecommenders-0.0.1-SNAPSHOT.jar
ttpathfiles=TrainTest
similaritiesFolder=Sim
jvmMemory=-Xmx3G


# Nomenclature for Datasets
EpinionsName=Epinions
MovieTweetings_600KName=MovieTweetings_600K
FoursqrName=Foursqr

# Extension of Files
extensionGlobalTrain=_global.train
extensionGlobalTest=_global.test
extensionPerUserTrain=_SplitPerUser.train
extensionPerUserTest=_SplitPerUser.test
extensionValidation=Validation
extensionParsedRemoveDuplicates=ParsedRemoveDuplicates

# Paths of Original Datasets
EpinionsRoute=OriginalDatasets/"$EpinionsName".dat
MovieTweetings_600KRoute=OriginalDatasets/"$MovieTweetings_600KName".dat
FoursqrRoute=OriginalDatasets/"$FoursqrName".dat

# Epinions
completeFileEpinionsUserItemFilter=OriginalDatasets/EpinionsParsedRemoveDuplicates2RatingsUserOrMore2RatingsItemsOrMore.txt
completeFileEpinions=$completeFileEpinionsUserItemFilter

# MovieTweetings
completeFileMovieTweetings_600KUserItemFilter=OriginalDatasets/MovieTweetings_600KParsedRemoveDuplicates5RatingsUserOrMore5RatingsItemsOrMore.txt
completeFileMovieTweetings_600K=$completeFileMovieTweetings_600KUserItemFilter

# Foursqr
completeFileFoursqrUserItemFilter=OriginalDatasets/FoursqrParsedRemoveDuplicates5RatingsUserOrMore5RatingsItemsOrMore.txt
completeFileFoursqr=$completeFileFoursqrUserItemFilter


javaCommand=java

mkdir -p $similaritiesFolder
mkdir -p $ttpathfiles


: '
  Install Maven
'
cd LCSBackwardForwardRecommenders
mvn install
cd ..


: '
    Processing Epinions dataset
'
# Prepare data for Epinions. Removing duplicates, creating new ids and creating k core for Epinions dataset
if [ ! -f OriginalDatasets/"$EpinionsName""$extensionParsedRemoveDuplicates".txt ]; then
   echo "Removing duplicates from Epinions dataset (DatasetTransform)"
   $javaCommand $jvmMemory -jar $JAR -o DatasetTransform -trf $EpinionsRoute OriginalDatasets/"$EpinionsName""$extensionParsedRemoveDuplicates".txt " "

   echo "All users and items must have at least 2 ratings in the final dataset (2-core) (DatasetReduction)"
   $javaCommand $jvmMemory -jar $JAR -o DatasetReduction -trf OriginalDatasets/"$EpinionsName""$extensionParsedRemoveDuplicates".txt $completeFileEpinions 2 2

fi

# Epinions Global split (full and validation)
if [ ! -f $ttpathfiles/"$EpinionsName"0"$extensionGlobalTrain" ]; then
    echo "Split for Epinions dataset (CC Train-Test). Creating Train and Test from complete dataset"
    # 80% to the train set, 20% to the test set
	$javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $completeFileEpinions $ttpathfiles/"$EpinionsName"0"$extensionGlobalTrain" $ttpathfiles/m"$EpinionsName"0"$extensionGlobalTest" 0.8

    echo "Split for Epinions dataset (CC Validation). Creating Train and Test validation from Training set"
    # Validation. Of the previous training set, we make another split. 80% train validation, 20% test validation
	$javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $ttpathfiles/"$EpinionsName"0"$extensionGlobalTrain" $ttpathfiles/"$EpinionsName""$extensionValidation"0"$extensionGlobalTrain" $ttpathfiles/m"$EpinionsName""$extensionValidation"0"$extensionGlobalTest" 0.8

fi

# Epinions Split per user split (full and validation)
if [ ! -f $ttpathfiles/"$EpinionsName"0"$extensionPerUserTrain" ]; then
    echo "Split for Epinions dataset (Fix Train-Test). Creating Train and Test from complete dataset. 2 leeast ratings for test. At least 4 items for train"
    $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $completeFileEpinions $ttpathfiles/"$EpinionsName"0"$extensionPerUserTrain" $ttpathfiles/m"$EpinionsName"0"$extensionPerUserTest" 2 4

    echo "Split for Epinions dataset (Fix Validation). Creating Train and Test from the train file. 2 last ratings for validation. At least 2 ratings for validation"
    $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $ttpathfiles/"$EpinionsName"0"$extensionPerUserTrain" $ttpathfiles/"$EpinionsName""$extensionValidation"0"$extensionPerUserTrain" $ttpathfiles/m"$EpinionsName""$extensionValidation"0"$extensionPerUserTest" 2 2
fi


: '
    Processing MovieTweetings dataset
'
# Prepare data for MovieTweetings. Removing duplicates, creating new ids and creating k core for MovieTweetings dataset (Although this dataset does not have repetitions, we work with our ids)
if [ ! -f OriginalDatasets/"$MovieTweetings_600KName""$extensionParsedRemoveDuplicates".txt ]; then
   echo "Removing duplicates from MovieTweetings dataset (DatasetTransform)"
   $javaCommand $jvmMemory -jar $JAR -o DatasetTransform -trf $MovieTweetings_600KRoute OriginalDatasets/"$MovieTweetings_600KName""$extensionParsedRemoveDuplicates".txt "::"

   echo "All users and items must have at least 5 ratings in the final dataset (5-core) (DatasetReduction)"
   $javaCommand $jvmMemory -jar $JAR -o DatasetReduction -trf OriginalDatasets/"$MovieTweetings_600KName""$extensionParsedRemoveDuplicates".txt $completeFileMovieTweetings_600K 5 5

fi

# MovieTweetings Global split (full and validation)
if [ ! -f $ttpathfiles/"$MovieTweetings_600KName"0"$extensionGlobalTrain" ]; then
    echo "Split for MovieTweetings dataset (CC Train-Test). Creating Train and Test from complete dataset"
    # 80% to the train set, 20% to the test set
	$javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $completeFileMovieTweetings_600K $ttpathfiles/"$MovieTweetings_600KName"0"$extensionGlobalTrain" $ttpathfiles/m"$MovieTweetings_600KName"0"$extensionGlobalTest" 0.8

    echo "Split for MovieTweetings dataset (CC Validation). Creating Train and Test validation from Training set"
    # Validation. Of the previous training set, we make another split. 80% train validation, 20% test validation
	$javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $ttpathfiles/"$MovieTweetings_600KName"0"$extensionGlobalTrain" $ttpathfiles/"$MovieTweetings_600KName""$extensionValidation"0"$extensionGlobalTrain" $ttpathfiles/m"$MovieTweetings_600KName""$extensionValidation"0"$extensionGlobalTest" 0.8

fi

# MovieTweetings Split per user split (full and validation)
if [ ! -f $ttpathfiles/"$MovieTweetings_600KName"0"$extensionPerUserTrain" ]; then
    echo "Split for MovieTweetings dataset (Fix Train-Test). Creating Train and Test from complete dataset. 2 leeast ratings for test. At least 4 items for train"
    $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $completeFileMovieTweetings_600K $ttpathfiles/"$MovieTweetings_600KName"0"$extensionPerUserTrain" $ttpathfiles/m"$MovieTweetings_600KName"0"$extensionPerUserTest" 2 4

    echo "Split for MovieTweetings dataset (Fix Validation). Creating Train and Test from the train file. 2 last ratings for validation. At least 2 ratings for validation"
    $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $ttpathfiles/"$MovieTweetings_600KName"0"$extensionPerUserTrain" $ttpathfiles/"$MovieTweetings_600KName""$extensionValidation"0"$extensionPerUserTrain" $ttpathfiles/m"$MovieTweetings_600KName""$extensionValidation"0"$extensionPerUserTest" 2 2
fi


: '
  Processing Foursqr dataset
'
if [ ! -f OriginalDatasets/"$FoursqrName""$extensionParsedRemoveDuplicates".txt ]; then
  echo "Removing duplicates from Foursqr dataset (DatasetTransform)"
  $javaCommand $jvmMemory -jar $JAR -o DatasetTransform -trf $FoursqrRoute OriginalDatasets/"$FoursqrName""$extensionParsedRemoveDuplicates".txt " "

  echo "All users and items must have at least 5 ratings (5-core) (DatasetReduction)"
  $javaCommand $jvmMemory -jar $JAR -o DatasetReduction -trf OriginalDatasets/"$FoursqrName""$extensionParsedRemoveDuplicates".txt $completeFileFoursqrUserItemFilter 5	5
fi

if [ ! -f $ttpathfiles/"$FoursqrName"0"$extensionGlobalTrain" ]; then
  echo "Split for Foursqr dataset (CC Train-Test). Creating Train and Test from complete dataset"
  # 80% to the train set, 20% to the test set
  $javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $completeFileFoursqr $ttpathfiles/"$FoursqrName"0"$extensionGlobalTrain" $ttpathfiles/m"$FoursqrName"0"$extensionGlobalTest" 0.8


  echo "Split for Foursqr dataset (CC Validation). Creating Train and Test validation from Training set"
  # Validation. Of the previous training set, we make another split. 80% train validation, 20% test validation
  $javaCommand $jvmMemory -jar $JAR -o TemporalGlobalSplit -trf $ttpathfiles/"$FoursqrName"0"$extensionGlobalTrain" $ttpathfiles/"$FoursqrName""$extensionValidation"0"$extensionGlobalTrain" $ttpathfiles/m"$FoursqrName""$extensionValidation"0"$extensionGlobalTest" 0.8

fi

# Foursqr Split per user split (full and validation)
if [ ! -f $ttpathfiles/"$FoursqrName"0"$extensionPerUserTrain" ]; then
  echo "Split for Foursqr dataset (Fix Train-Test). Creating Train and Test from complete dataset. 2 last ratings for test. At least 4 items for train"
  $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $completeFileFoursqr $ttpathfiles/"$FoursqrName"0"$extensionPerUserTrain" $ttpathfiles/m"$FoursqrName"0"$extensionPerUserTest" 2 4

  echo "Split for Foursqr dataset (Fix Validation). Creating Train and Test from the train file. 2 last ratings for validation. At least 2 ratings for validation"
  $javaCommand $jvmMemory -jar $JAR -o TemporalSplitPerUser -trf $ttpathfiles/"$FoursqrName"0"$extensionPerUserTrain" $ttpathfiles/"$FoursqrName""$extensionValidation"0"$extensionPerUserTrain" $ttpathfiles/m"$FoursqrName""$extensionValidation"0"$extensionPerUserTest" 2 2
fi

# Download mymedialite and unzip it
if [ ! -d "MyMediaLite-3.11" ]; then
  wget "http://mymedialite.net/download/MyMediaLite-3.11.tar.gz"
  tar zxvf MyMediaLite-3.11.tar.gz
fi

# Execute Fossil makefile
if [ ! -f "Fossil_RankingModification/train" ]; then
  make -C "Fossil_RankingModification/"
fi
