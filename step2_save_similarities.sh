#!/bin/sh

: '
  Second split:
  Save similarities in order to be read by the framework. All similarities to be used with the BF indexes need to be stored
'

# Global variables
JAR=LCSBackwardForwardRecommenders/target/LCSBackwardForwardRecommenders-0.0.1-SNAPSHOT.jar
ttpathfiles=TrainTest
similaritiesFolder=Sim
jvmMemory=-Xmx16G


# Nomenclature for Datasets
EpinionsName=Epinions
MovieTweetings_600KName=MovieTweetings_600K
FoursqrName=Foursqr

# Extension of Files
extensionGlobalTrain=_global.train
extensionGlobalTest=_global.test
extensionPerUserTrain=_SplitPerUser.train
extensionPerUserTest=_SplitPerUser.test
extensionValidation=Validation

ubsimPrefix=sim

javaCommand=java


: '
 Saving similarities. Epinions
'
for lcsnormalization in NONORMALIZATION
do

  for lcsTrain in ITEMID_ORDEREDBY_TIMESTAMP
  do

    # Full and Validation splitting
    for fold in "$EpinionsName"0 "$EpinionsName""$extensionValidation"0 "$MovieTweetings_600KName"0 "$MovieTweetings_600KName""$extensionValidation"0 "$FoursqrName"0 "$FoursqrName""$extensionValidation"0
    do

      echo "Creating NONORMALIZATION file for $fold . Global Split"

      trainfile=$ttpathfiles/"$fold""$extensionGlobalTrain"
      testfile=$ttpathfiles/m$fold"$extensionGlobalTest"

      outputSimfile=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"GLOBAL"_"$lcsTrain"_LCSNorm"$lcsnormalization".txt

      $javaCommand $jvmMemory -jar $JAR -o lcs4recsysSaveUBSimFile -trf $trainfile -osf $outputSimfile -lcsn $lcsnormalization -lcstm $lcsTrain

      # Now create the norm files from the previous created one
      for lcsnormalizationCreatingNormFile in LCSPOW2DIVMULT LCSPLUSLCSDIVSUM
      do
        outputSimFileNormCreation=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"GLOBAL"_"$lcsTrain"_LCSNorm"$lcsnormalizationCreatingNormFile".txt

        $javaCommand $jvmMemory -jar $JAR -o lcs4recsysCreateNormFile -trf $trainfile -osf $outputSimFileNormCreation -lcsn $lcsnormalizationCreatingNormFile -lcstm $lcsTrain -sf $outputSimfile

      done
      wait # End normalizations

      # RankSys store similarities
      for UBsimilarity in VectorCosineUserSimilarity VectorJaccardUserSimilarity SetJaccardUserSimilarity
      do
        outputSimFile=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"GLOBAL"_RankSysStore_"$UBsimilarity".txt
        $javaCommand $jvmMemory -jar $JAR  -o ranksysSaveUBSimFile -trf $trainfile -tsf $testfile -osf $outputSimFile -rs $UBsimilarity
      done
      wait # End RankSysSimilarities

      ### PER USER
      # Split per user
      echo "Creating NONORMALIZATION file for $fold. Split Per user"

      trainfile=$ttpathfiles/"$fold""$extensionPerUserTrain"
      testfile=$ttpathfiles/m$fold"$extensionPerUserTest"

      outputSimfile=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"PERUSER"_"$lcsTrain"_LCSNorm"$lcsnormalization".txt

      $javaCommand $jvmMemory -jar $JAR -o lcs4recsysSaveUBSimFile -trf $trainfile -osf $outputSimfile -lcsn $lcsnormalization -lcstm $lcsTrain

      # Now create the norm files from the previous created one
      for lcsnormalizationCreatingNormFile in LCSPOW2DIVMULT LCSPLUSLCSDIVSUM
      do
        outputSimFileNormCreation=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"PERUSER"_"$lcsTrain"_LCSNorm"$lcsnormalizationCreatingNormFile".txt

        $javaCommand $jvmMemory -jar $JAR -o lcs4recsysCreateNormFile -trf $trainfile -osf $outputSimFileNormCreation -lcsn $lcsnormalizationCreatingNormFile -lcstm $lcsTrain -sf $outputSimfile

      done
      wait # End Nomalizations

      # RankSys store similarities
      for UBsimilarity in VectorCosineUserSimilarity VectorJaccardUserSimilarity SetJaccardUserSimilarity
      do
        outputSimFile=$similaritiesFolder/"$ubsimPrefix"_"$fold"_"PERUSER"_RankSysStore_"$UBsimilarity".txt
        $javaCommand $jvmMemory -jar $JAR  -o ranksysSaveUBSimFile -trf $trainfile -tsf $testfile -osf $outputSimFile -rs $UBsimilarity
      done
      wait # End RankSysSimilarities

    done # End fold
    wait

  done # End train type
  wait

done # End normalization
wait
