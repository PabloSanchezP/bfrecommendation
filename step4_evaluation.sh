#!/bin/sh

# Description
: '
Evaluation of the recommenders generated in the previous step
'

# Java variables
JAR=LCSBackwardForwardRecommenders/target/LCSBackwardForwardRecommenders-0.0.1-SNAPSHOT.jar
jvmMemory=-Xmx8G

# Files/paths variables
ttpathfiles=TrainTest
recommendedFolder=RecommendationFolder
mkdir -p $recommendedFolder
similaritiesFolder=Sim
datapathfiles=TrainTest
ubsimPrefix=sim
ibsimPrefix=ibsim
recPrefix=rec
resultsPrefix=eval
naresultsPrefix=naeval

ItemsTimeFiles=ItemsTimeFiles
mkdir -p $ItemsTimeFiles

# Evaluation variables
itemsRecommended=200
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
alltemporalLambdas="0.1 0.05 0.02 0.01" #Values from paper of Ding 2005
allindexesBackwardForward="2 5 10"
allRuiningHEKs="2 5 10"
allRuiningHELs="1 2 3"
allRuiningHeLambdas="0.1 0.2"
allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10 100"


javaCommand=java

# Nomenclature for Datasets
EpinionsName=Epinions
FoursqrName=Foursqr
MovieTweetings_600KName=MovieTweetings_600K

# Extension of Files
extensionGlobalTrain=_global.train
extensionGlobalTest=_global.test
extensionPerUserTrain=_SplitPerUser.train
extensionPerUserTest=_SplitPerUser.test
extensionValidation=Validation
extensionParsedRemoveDuplicates=extensionParsedRemoveDuplicates


# MyMediaLite variables
mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1 0.0005"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01 0.00005"
extensionMyMediaLite=MyMedLt



#
ValidationResults=ResultRankSysValidationNoBinRelNoDisc
FullResults=ResultRankSysFullNoBinRelNoDisc

mkdir -p ResultRankSysValidationNoBinRelNoDisc
mkdir -p ResultRankSysFullNoBinRelNoDisc

for fold in "$EpinionsName"0 "$EpinionsName""$extensionValidation"0 "$MovieTweetings_600KName"0 "$MovieTweetings_600KName""$extensionValidation"0 "$FoursqrName"0 "$FoursqrName""$extensionValidation"0
do
	for splitType in GLOBAL PERUSER
	do
		trainfile=""
		testfile=""

		if [[ "$splitType" == "GLOBAL" ]]; then
			echo "GlobalSplit"
			trainfile=$ttpathfiles/"$fold""$extensionGlobalTrain"
			testfile=$ttpathfiles/m$fold"$extensionGlobalTest"
		fi
		if [[ "$splitType" == "PERUSER" ]]; then
			echo "PerUserSplit"
			trainfile=$ttpathfiles/"$fold""$extensionPerUserTrain"
			testfile=$ttpathfiles/m$fold"$extensionPerUserTest"
		fi

		# Depending on the dataset, we use different evaluation thresholds
		evthreshold=0
		if [[ $fold = *"$EpinionsName"* ]]; then
			evthreshold=5
		fi
		if [[ $fold = *"$MovieTweetings_600KName"* ]]; then
			evthreshold=9
		fi
		if [[ $fold = *"$FoursqrName"* ]]; then
			evthreshold=1
		fi

		if [[ $fold = *"$extensionValidation"* ]]; then
			resultsFolder="$ValidationResults"
		else
			resultsFolder="$FullResults"
		fi

		# Each file of Recomendations

		find $recommendedFolder/ -name "$recPrefix"_"$fold"_"$splitType"_* | while read recFile; do
			recFileName=$(basename "$recFile" .txt) #extension removed

			for cutoff in 5
			do

					outputResultfile=$resultsFolder/"$naresultsPrefix"_EvTh"$evthreshold"_"$recFileName"_"$cutoff".txt
					# Cutoff and rankSys result feature file need to be parametrized
					$javaCommand -jar $JAR -o ranksysNonAccuracyWithoutFeatureMetricsEvaluation -trf $trainfile -tsf $testfile -rf $recFile -thr $evthreshold -rc $cutoff -orf $outputResultfile -ItimeAvgFile "$ItemsTimeFiles"/"$splitType""$fold""AvgTimeItemsfile.txt" -ItimeFstFile "$ItemsTimeFiles"/"$splitType""$fold""FstTimeItemsfile.txt" -ItimeMedFile "$ItemsTimeFiles"/"$splitType""$fold""MedTimeItemsfile.txt" -ItimeLastFile "$ItemsTimeFiles"/"$splitType""$fold""LastTimeItemsfile.txt"

			done # End cutoff
			wait
		done # End find
		wait


		cutoff=5
		# Average for HKV
		for rankRecommenderNoSim in MFRecommenderHKV
		do
			for kFactor in $allKFactorizerRankSys
			do
				for lambdaValue in $allLambdaFactorizerRankSys
				do
					for alphaValue in $allAlphaFactorizerRankSys
					do

						# Neighbours is put to 20 because this recommender does not use it
						matchingFile="$recPrefix"_"$fold"_"$splitType"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_LFactorizer"$lambdaValue"
						echo $matchingFile
						for folderResults in $ValidationResults $BinRelResults
						do
							outputResultfile=$resultsFolder/"$naresultsPrefix"_EvTh"$evthreshold"_"$matchingFile"_AVERAGE_"$cutoff".txt
							$javaCommand -jar $JAR -o AverageSetOfResults -trf $folderResults/ $matchingFile $outputResultfile
						done # End folder results
						wait
					done # End alpha
					wait
				done # End lambda
				wait
			done # End kFactor
			wait
		done
		wait # End HKV

		# Average for BPRMF
		for factor in $BPRFactors
		do
			for bias_reg in $BPRBiasReg
			do
				for regU in $BPRRegU # Regularization for items and users is the same
				do
					regJ=$(echo "$regU/10" | bc -l)

					matchingFile="$recPrefix"_"$fold"_"$splitType"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ"
					echo $matchingFile
					outputResultfile=$resultsFolder/"$naresultsPrefix"_EvTh"$evthreshold"_"$matchingFile"_AVERAGE_"$cutoff".txt
					$javaCommand -jar $JAR -o AverageSetOfResults -trf $folderResults/ $matchingFile $outputResultfile
				done # End regU
				wait
			done # End bias_reg
			wait
		done # End factor
		wait

		# Average all He files
		for HeK in $allRuiningHEKs
		do
			for HeLambda in $allRuiningHeLambdas
			do
				for HeL in $allRuiningHELs
				do
					for recommmender in Fossil mc fpmc
					do
						matchingFile="$recPrefix"_"$fold"_"$splitType"_"$recommmender"_K"$HeK"_Lambda"$HeLambda"_L"$HeL"
						echo $matchingFile
						outputResultfile=$resultsFolder/"$naresultsPrefix"_EvTh"$evthreshold"_"$matchingFile"_AVERAGE_"$cutoff".txt
						$javaCommand -jar $JAR -o AverageSetOfResults -trf $folderResults/ $matchingFile $outputResultfile
					done # End recommmender
					wait
				done # End He L
				wait
			done # End He lambda
			wait
		done # End HeK
		wait

	done # End split
	wait

done # End fold
wait
