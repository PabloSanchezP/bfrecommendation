#pragma once

#include "model.hpp"

class FISMrmse : public model
{
public:
	FISMrmse(corpus* corp, int K, double rho, double alpha, double lambda, double bias_reg) 
			: model(corp)
			, K(K)
			, rho(rho)
			, alpha(alpha)
			, lambda(lambda)
			, bias_reg(bias_reg) {}

	~FISMrmse(){}
	
	void init();
	void cleanUp();

	double prediction(int user, int item_prev, int item);
	void getParametersFromVector(	double*   g,
									double**  beta_u, 
									double**  beta_i,
									double*** P,
									double*** Q,
									action_t action);

	void train(int iterations, double learn_rate);
	double oneiteration(double learn_rate);
	double updateFactors(int user, int item, float val, double learn_rate);
	string toString();

	/* auxiliary variables */
	double* beta_u;
	double* beta_i;
	double** P;
	double** Q;

	/* hyper-parameters */
	int K;
	double rho;
	double alpha;
	double lambda;
	double bias_reg;
};