#pragma once

#include "model.hpp"

class FISMauc : public model
{
public:
	FISMauc(corpus* corp, int K, double rho, double alpha, double lambda, double bias_reg) 
			: model(corp)
			, K(K)
			, rho(rho)
			, alpha(alpha)
			, lambda(lambda)
			, bias_reg(bias_reg) {}

	~FISMauc(){}
	
	void init();
	void cleanUp();

	double prediction(int user, int item_prev, int item);
	void getParametersFromVector(	double*   g,
									double**  beta_i,
									double*** P,
									double*** Q,
									action_t action);

	void train(int iterations, double learn_rate);
	double oneiteration(double learn_rate);
	string toString();

	void updateFactors(int user, int i, int j, double learn_rate);

	/* auxiliary variables */
	double* beta_i;
	double** P;
	double** Q;

	/* hyper-parameters */
	int K;
	double rho;
	double alpha;
	double lambda;
	double bias_reg;
};