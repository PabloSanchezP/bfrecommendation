#!/bin/sh

: '
	File that allows us to obtain summaries of the results. We will obtain 4 files for each dataset.
	- For validation (per user split)
	- For validation (global split)
	- Full (per user split)
	- Full (global split)
'

# Files/paths variables
EpinionsName=Epinions
FoursqrName=Foursqr
MovieTweetings_600KName=MovieTweetings_600K


nonaccresultsPrefix=naeval
extensionValidation=Validation
subsStringMatching="_"

ValidationResults=ResultRankSysValidationNoBinRelNoDisc
FullResults=ResultRankSysFullNoBinRelNoDisc



## Epinions
nasummaryFileGLOBALEpinionsValidation=summaryEpinionsValidationGLOBAL.dat
nasummaryFilePERUSEREpinionsValidation=summaryEpinionsValidationPERUSER.dat
nasummaryFileGLOBALEpinionsFull=summaryEpinionsFullGLOBAL.dat
nasummaryFilePERUSEREpinionsFull=summaryEpinionsFullPERUSER.dat
rm $nasummaryFileGLOBALEpinionsValidation
rm $nasummaryFilePERUSEREpinionsValidation
rm $nasummaryFileGLOBALEpinionsFull
rm $nasummaryFilePERUSEREpinionsFull

for fold in "$EpinionsName"
do
		for cutoff in 5
		do
			for evthreshold in 5
			do
				# Validation Global
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALEpinionsValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Validation PerUser
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSEREpinionsValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Full Global
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALEpinionsFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Full PerUser
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSEREpinionsFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

			done # End evTh
			wait
		done # End CutOff
		wait
done # End fold
wait

## MovieTweetings
nasummaryFileGLOBALMovieTweetings_600KValidation=summaryMovieTweetings_600KValidationGLOBAL.dat
nasummaryFilePERUSERMovieTweetings_600KValidation=summaryMovieTweetings_600KValidationPERUSER.dat
nasummaryFileGLOBALMovieTweetings_600KFull=summaryMovieTweetings_600KFullGLOBAL.dat
nasummaryFilePERUSERMovieTweetings_600KFull=summaryMovieTweetings_600KFullPERUSER.dat
rm $nasummaryFileGLOBALMovieTweetings_600KValidation
rm $nasummaryFilePERUSERMovieTweetings_600KValidation
rm $nasummaryFileGLOBALMovieTweetings_600KFull
rm $nasummaryFilePERUSERMovieTweetings_600KFull

for fold in "$MovieTweetings_600KName"
do
		for cutoff in 5
		do
			for evthreshold in 9
			do
				# Validation Global
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALMovieTweetings_600KValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done #End find
				wait

				# Validation PerUser
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSERMovieTweetings_600KValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done #End find
				wait

				# Full Global
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALMovieTweetings_600KFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Full PerUser
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSERMovieTweetings_600KFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

			done # End evTh
			wait
		done # End CutOff
		wait
done # End fold
wait

## Foursqr
nasummaryFileGLOBALFoursqrValidation=summaryFoursqrValidationGLOBAL.dat
nasummaryFilePERUSERFoursqrValidation=summaryFoursqrValidationPERUSER.dat
nasummaryFileGLOBALFoursqrFull=summaryFoursqrFullGLOBAL.dat
nasummaryFilePERUSERFoursqrFull=summaryFoursqrFullPERUSER.dat
rm $nasummaryFileGLOBALFoursqrValidation
rm $nasummaryFilePERUSERFoursqrValidation
rm $nasummaryFileGLOBALFoursqrFull
rm $nasummaryFilePERUSERFoursqrFull

for fold in "$FoursqrName"
do
		for cutoff in 5
		do
			for evthreshold in 1
			do
				# Validation Global
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALFoursqrValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Validation PerUser
				find "$ValidationResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSERFoursqrValidation -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Full Global
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"GLOBAL"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFileGLOBALFoursqrFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

				# Full PerUser
				find "$FullResults"/ -name "$nonaccresultsPrefix"_EvTh"$evthreshold"*"$fold"*"PERUSER"*"$cutoff"* | while read recFile; do
					recFileName=$(basename "$recFile" .txt) #extension removed
					awk -v FILE=$nasummaryFilePERUSERFoursqrFull -v TH=$evthreshold -v AT=$cutoff -v RNAME=$recFileName -v FOLD=$fold 'BEGIN { RS = "\n" } ; { print FILENAME"\t"RNAME"\t"FOLD"\t"AT"\t"TH"\t"$0 >> FILE }' $recFile
				done # End find
				wait

			done # End evTh
			wait
		done # End CutOff
		wait
done # End fold
wait
