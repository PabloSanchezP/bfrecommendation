/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.lcs;

import es.uam.eps.ir.bfsequential.utils.Tuple3D;

/***
 * Final class that contains the methods to compute the LCS
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class LongestCommonSubsequence {

	/***
	 * Method to compute the LCS between 2 array of integers
	 * 
	 * @param x
	 *            the integer array associated to user 1
	 * @param y
	 *            the integer array associated to user 2
	 * @param matchingThreshold
	 *            the matching threshold associated
	 * @return a tuple containing the similarity and the two indexes
	 */
	public static Tuple3D<Double, Integer, Integer> longestCommonSubsequenceArrays(Integer[] x, Integer[] y,
			int matchingThreshold) {
		int m = x.length;
		int n = y.length;
		int[][] c = new int[m + 1][n + 1];
		int finalIndexi = 0;
		int finalIndexj = 0;

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if ((Math.abs(x[i] - y[j]) <= matchingThreshold)) {
					c[i + 1][j + 1] = c[i][j] + 1;
					finalIndexi = Math.max(i, finalIndexi);
					finalIndexj = Math.max(j, finalIndexj);
				} else
					c[i + 1][j + 1] = Math.max(c[i + 1][j], c[i][j + 1]);
			}
		}

		return new Tuple3D<Double, Integer, Integer>((double) c[m][n], finalIndexi, finalIndexj);
	}

}
