/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.sim.lcs;

import java.util.HashSet;
import java.util.Map;

import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.sim.AbstractSimilarity;
import es.uam.eps.ir.bfsequential.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.bfsequential.utils.Tuple3D;

/***
 * Abstract class of LCSSimilarity. It can normalize the LCS result in 4
 * different ways. Possible normalizations are (for arrays x and y):
 * 
 * -NONORMALIZATION: returns the original value of LCS(x,y).
 * 
 * -LCSDIVMAX: returns the LCS(x,y) / Max(length(x), length(y))
 * 
 * -LCSDIVMIN: returns the LCS(x,y) / Min(length(x), length(y))
 * 
 * -LCSPLUSLCSDIVSUM: returns the LCS(x,y)*2 / (length(x) + length(y))
 * 
 * -LCSPOW2DIVMULT: returns the LCS(x,y)*LCS(x,y) / (length(x) * length(y))
 * 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public abstract class LCSSimilarity extends AbstractSimilarity {

	public enum LCSNormalization {
		NONORMALIZATION, LCSDIVMAX, LCSDIVMIN, LCSPLUSLCSDIVSUM, LCSPOW2DIVMULT
	};

	public LCSSimilarity() {
		this.totalElements = new HashSet<Long>();
	}

	/**
	 * Constructor that loads all the similarities from a file
	 * 
	 * @param path
	 */
	public LCSSimilarity(String path) {
		super(path);
	}

	/**
	 * Same constructor as before, but receiving a preference. If the similarity
	 * 
	 * @param path
	 * @param confidence
	 */
	public LCSSimilarity(String path, Double confidence) {
		super(path, confidence);
	}

	/***
	 * Constructor that receives a map to store the similarities.
	 * 
	 * @param outputPath
	 * @param map
	 */
	public LCSSimilarity(String outputPath, Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> map) {
		similarities = map;
		desc = outputPath;
	}

	/***
	 * Normalization methods. It one of the arguments is 0 or empty, the result will
	 * be 0.
	 * 
	 * @param norm
	 *            the enumeration normalization
	 * @param lcs
	 *            the value of the lcs computed
	 * @param lcsel1
	 *            the first array of integers
	 * @param lcsel2
	 *            the second array of integers
	 * @return the normalization.
	 */
	public double normalizeLCS(DataModelIF<Long, Long> dataModel, LCSNormalization norm,
			Tuple3D<Double, Integer, Integer> lcsResult, Long element1, Long element2,
			Map<Long, Integer[]> transformation) {
		Integer[] transformation1 = transformation.get(element1);
		Integer[] transformation2 = transformation.get(element2);

		if (lcsResult.item1 == 0 || transformation1.length == 0 || transformation2.length == 0)
			return 0;
		switch (norm) {
		case NONORMALIZATION:
			return lcsResult.item1;
		case LCSDIVMAX:
			return lcsResult.item1 / (Math.max((double) transformation1.length, (double) transformation2.length));

		case LCSDIVMIN:
			return lcsResult.item1 / (Math.min((double) transformation1.length, (double) transformation2.length));

		case LCSPLUSLCSDIVSUM:
			return lcsResult.item1 * 2 / ((double) transformation1.length + (double) transformation2.length);

		case LCSPOW2DIVMULT:
			return lcsResult.item1 * lcsResult.item1 / (transformation1.length * transformation2.length);
		default: // Default will be no normalization
			return lcsResult.item1;
		}
	}

	@Override
	public boolean isSymmetric() {
		return true;
	}

	@Override
	public Tuple3D<Double, Integer, Integer> completeSimilarity(Long first, Long second) {
		SymmetricSimilarityBean<Long> s = new SymmetricSimilarityBean<Long>(first, second);
		if (similarities.get(s) == null) {
			return new Tuple3D<Double, Integer, Integer>(Double.NaN, -1, -1);
		}

		return new Tuple3D<Double, Integer, Integer>(similarities.get(s).item1, similarities.get(s).item2,
				similarities.get(s).item3);
	}

	@Override
	public void load(String path) {
		loadConfidence(path, Double.NaN);
	}

	public void setElement1Index(int index) {
		this.element1index = index;
	}

	public void setElement2Index(int index) {
		this.element2index = index;
	}

	public void setElementsimIndex(int simIndex) {
		this.simIndex = simIndex;
	}

}
