/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.sim;

import java.util.Map;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.bfsequential.utils.Tuple3D;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/**
 * Our implementation of a user-based similarity that allows to load from file.
 * It also stores a third value to keep track of the last interaction between
 * two users.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class UBSimilarity extends AbstractSimilarity implements Similarity {

	public UBSimilarity() {

	}

	public UBSimilarity(String path) {
		super(path, Double.NaN);
	}

	public UBSimilarity(String path, Double confidence) {
		super(path, Double.NaN);
	}

	public UBSimilarity(Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> similarities) {
		this.similarities = similarities;
	}

	@Override
	public IntToDoubleFunction similarity(int idx) {
		return null;
	}

	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		return null;
	}

}
