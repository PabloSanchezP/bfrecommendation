/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.sim;

import java.util.function.IntToDoubleFunction;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;

/**
 * User-based similarity for RankSys based on UBSimilarity
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 */
public class UBSimilarityRankSys extends UserSimilarity<Long> {

	public UBSimilarityRankSys(FastPreferenceData<Long, Long> data, String path, Double confidence) {
		super(data, new UBSimilarity(path, confidence) {
			@Override
			public Stream<Tuple2id> similarElems(int idx) {
				Long first = data.uidx2user(idx);
				return getSimilarities(first).entrySet().stream().map(e -> {
					int idx2 = data.user2uidx(e.getKey());
					return new Tuple2id(idx2, e.getValue());
				});
			}

			@Override
			public IntToDoubleFunction similarity(int idx1) {
				Long first = data.uidx2user(idx1);
				return idx2 -> {
					Long second = data.uidx2user(idx2);
					return getSimilarity(first, second);
				};
			}
		});
	}

}
