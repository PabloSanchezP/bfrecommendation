/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.sim.lcs;

import java.util.HashMap;
import java.util.Map;
import java.util.function.IntToDoubleFunction;
import java.util.stream.Stream;

import org.ranksys.core.util.tuples.Tuple2id;

import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.core.GenericUser;
import es.uam.eps.ir.bfsequential.lcs.LongestCommonSubsequence;
import es.uam.eps.ir.bfsequential.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.bfsequential.utils.LCSParser;
import es.uam.eps.ir.bfsequential.utils.Tuple3D;
import es.uam.eps.ir.bfsequential.utils.LCSParser.UBTrainModel;
import es.uam.eps.ir.ranksys.nn.sim.Similarity;

/**
 * LCSUser Similarity. Computes LCS between users depending on the trainModel to
 * use In UB, the users are transformed using the items that he/she has rated.
 * 
 * As we have integrated other Frameworks like RankSys, this class must
 * implement the interfaces of similarities of that framework.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class LCSUserSimilarity extends LCSSimilarity implements Similarity {

	/***
	 * Constructor that not receive the preference value. It calls the constructor
	 * that receives that value with a 0 by default. Preference ALL means that all
	 * ratings are considered
	 * 
	 * @param dataModel
	 *            the datamodel of the application
	 * @param lcsModel
	 *            the lcs training model.
	 * @param threshold
	 *            the threshold of the rating to consider them similar
	 * @param lcsNormalization
	 *            the normalization that we will apply to LCS result.
	 */
	public LCSUserSimilarity(DataModelIF<Long, Long> dataModel, UBTrainModel lcsModel, int threshold,
			LCSNormalization lcsNormalization) {
		this(dataModel, dataModel, lcsModel, threshold, lcsNormalization, 100, 10);
	}

	/***
	 * Same constructor but it receives 2 datamodels, one for train and one for
	 * test.
	 * 
	 * @param dataModel
	 *            the datamodel of the application
	 * @param dataModel2
	 *            the second datamodel (maybe test file)
	 * @param lcsModel
	 *            the lcs training model.
	 * @param threshold
	 *            the threshold of the rating to consider them similar
	 * @param lcsNormalization
	 *            the normalization that we will apply to LCS result.
	 */
	public LCSUserSimilarity(DataModelIF<Long, Long> dataModel, DataModelIF<Long, Long> dataModel2,
			UBTrainModel lcsModel, int threshold, LCSNormalization lcsNormalization) {
		this(dataModel, dataModel2, lcsModel, threshold, lcsNormalization, 100, 10);
	}

	/**
	 * Same constructor as before, with a preference value.
	 * 
	 * @param dataModel
	 * @param lcsModel
	 * @param threshold
	 * @param lcsNormalization
	 * @param preference
	 */
	public LCSUserSimilarity(DataModelIF<Long, Long> dataModel, DataModelIF<Long, Long> dataModel2,
			UBTrainModel lcsModel, int threshold, LCSNormalization lcsNormalization, int idFactor, int ratingFactor) {

		super();

		Map<Long, Integer[]> transformation = LCSParser.userToLCSTransformation(dataModel, lcsModel, idFactor,
				ratingFactor);
		similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>>();

		computeSimilarities(dataModel, dataModel2, lcsModel, transformation, threshold, lcsNormalization);

		desc = buildDesc(lcsModel, threshold, lcsNormalization);
	}

	/**
	 * User similarities when reading non normalization file.
	 * 
	 * @param dataModel
	 * @param lcsModel
	 * @param lcsNormalizarion
	 * @param fileSimilarities
	 * @param ratingFactor
	 * @param idFactor
	 */
	public LCSUserSimilarity(DataModelIF<Long, Long> dataModel, UBTrainModel lcsModel,
			LCSNormalization lcsNormalizarion, String fileSimilarities, int idFactor, int ratingFactor) {
		super();

		Map<Long, Integer[]> transformation = LCSParser.userToLCSTransformation(dataModel, lcsModel, idFactor,
				ratingFactor);
		similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>>();

		computeSimilarities(dataModel, fileSimilarities, transformation, lcsNormalizarion);

	}

	/***
	 * Method to compute the similarities using a not normalization file
	 * 
	 * @param dataModel
	 *            the dataModel
	 * @param path
	 *            path of the Not normalized file
	 * @param transformation
	 *            the transformation of the items
	 * @param ratTransformation
	 *            the rating transformation
	 * @param lcsNormalizarion
	 *            the normalization approach of LCS
	 */
	private void computeSimilarities(DataModelIF<Long, Long> dataModel, String path,
			Map<Long, Integer[]> transformation, LCSNormalization lcsNormalizarion) {
		this.load(path);
		Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> newSimilarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>>();
		for (SymmetricSimilarityBean<Long> sSB : similarities.keySet()) {
			SymmetricSimilarityBean<Long> sSBNew = new SymmetricSimilarityBean<Long>(sSB.getElement1(),
					sSB.getElement2());
			double simNew = normalizeLCS(dataModel, lcsNormalizarion, similarities.get(sSB), sSB.getElement1(),
					sSB.getElement2(), transformation);
			Tuple3D<Double, Integer, Integer> tNew = new Tuple3D<Double, Integer, Integer>(simNew,
					similarities.get(sSB).item2, similarities.get(sSB).item3);
			newSimilarities.put(sSBNew, tNew);
		}
		// At the end, we substitute previous map of similarities with the new one
		this.similarities = newSimilarities;
	}

	public LCSUserSimilarity(String path) {
		super(path);
	}

	public LCSUserSimilarity(String path, Double confidenceThreshold) {
		super(path, confidenceThreshold);
	}

	public LCSUserSimilarity(String path, Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> map) {
		super(path, map);
	}

	/**
	 * Auxiliary train method. It computes the similarities of each user.
	 * 
	 * @param transformation
	 *            the transformation of that user
	 * @param auxMapSimilarities
	 *            map used to store the similarities
	 */
	private void computeSimilarities(DataModelIF<Long, Long> dataModel1, DataModelIF<Long, Long> dataModel2,
			UBTrainModel lcsModel, Map<Long, Integer[]> transformation, int threshold,
			LCSNormalization lcsNormalization) {
		int countUsers = 0;

		/*
		 * DataModel 2 can be the test Datamodel. That means that we dont need to
		 * compute the complete matrix of similarities, only the ones that appears in
		 * test.
		 */
		for (Long user : dataModel2.getUsers().keySet()) {
			countUsers++;

			// Debug
			if (countUsers % 5 == 0)
				System.out.println(countUsers + " out of " + dataModel2.getUsers().keySet().size());
			for (Long user2 : dataModel1.getUsers().keySet()) {
				if (user.equals(user2)) // If they are the same
					continue;

				// If similarity does no exist in the map, we calculate it and
				// add it
				SymmetricSimilarityBean<Long> s = new SymmetricSimilarityBean<Long>(user, user2);

				if (similarities.get(s) == null) {
					Tuple3D<Double, Integer, Integer> simV = computeLCS(dataModel1, lcsModel, transformation, user,
							user2, threshold);
					double sim = normalizeLCS(dataModel1, lcsNormalization, simV, user, user2, transformation);
					simV.item1 = sim;

					/*
					 * If a LCS Approach have a similarity of 0 we should not consider it. But we
					 * can compute pearson correlation and cosine similarity so a 0 in that
					 * similarities make more sense
					 */
					if (!Double.isNaN(sim) && sim > 0)
						similarities.put(s, simV);

				}
			}
		}
	}

	/***
	 * Method to compute LCS. depending on the train model, in UB we can compute LCS
	 * in different ways. When operating with bitmask, when operating with an alpha
	 * and finally when operating normally.
	 * 
	 * @param lcsModel
	 *            the Datamodel
	 * @param transformation
	 *            the map f transformations
	 * @param user1
	 *            the first user
	 * @param user2
	 *            the second user
	 * @param threshold
	 * @return
	 */
	public static Tuple3D<Double, Integer, Integer> computeLCS(DataModelIF<Long, Long> dataModel, UBTrainModel lcsModel,
			Map<Long, Integer[]> transformation, Long user1, Long user2, int threshold) {
		// Initialization of variables
		Integer[] trsnformU1 = transformation.get(user1);
		Integer[] trsnformU2 = transformation.get(user2);
		GenericUser<Long, Long> u1 = dataModel.getUsers().get(user1);
		GenericUser<Long, Long> u2 = dataModel.getUsers().get(user2);

		if (trsnformU1 == null || trsnformU2 == null || u1 == null || u2 == null)
			return new Tuple3D<Double, Integer, Integer>(0.0, -1, -1);

		return LongestCommonSubsequence.longestCommonSubsequenceArrays(trsnformU1, trsnformU2, threshold);
	}

	@Override
	public String toString() {
		return desc;
	}

	private String buildDesc(UBTrainModel model, int threshold, LCSNormalization lcsNormalization) {
		String s = "UB_LCSSim_" + model + "_Threshold_" + threshold + "_" + lcsNormalization;
		return s;
	}

	@Override
	public String info() {
		return desc;
	}

	/* Ranksys's Method */
	@Override
	public Stream<Tuple2id> similarElems(int idx) {
		return null;
	}

	/* Ranksys's method */
	@Override
	public IntToDoubleFunction similarity(int idx1) {
		return null;
	}

}
