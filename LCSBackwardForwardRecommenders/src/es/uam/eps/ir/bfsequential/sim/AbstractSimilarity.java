/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.sim;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import es.uam.eps.ir.bfsequential.utils.Tuple3D;

/***
 * Abstract similarity storing indexes and symmetric similarities values i.e.
 * sim(u,v) = sim (v,u)
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class AbstractSimilarity implements SimilarityIF<Long> {
	protected String desc;
	protected Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> similarities; // Map of similarities
																									// between elements.
																									// Its symmetric.

	protected int element1index = 0;// The indexes of the file can be changed using setters
	protected int element2index = 1;
	protected int simIndex = 2;
	protected Set<Long> totalElements;

	public AbstractSimilarity() {

	}

	public AbstractSimilarity(String path) {
		this(path, Double.NaN);
	}

	public AbstractSimilarity(String path, Double confidence) {
		similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>>();
		totalElements = new LinkedHashSet<>();
		loadConfidence(path, confidence);
		desc = new File(path).getName();
	}

	@Override
	public boolean isSymmetric() {
		return true;
	}

	@Override
	public Double getSimilarity(Long first, Long second) {
		SymmetricSimilarityBean<Long> s = new SymmetricSimilarityBean<Long>(first, second);
		if (similarities.get(s) == null)
			return Double.NaN;

		return similarities.get(s).item1;
	}

	@Override
	public Map<Long, Double> getSimilarities(Long first) {
		if (totalElements == null || totalElements.size() == 0)
			return getNormalSimilarities(first);
		else
			return getEfficientSimilarities(first);
	}

	private Map<Long, Double> getNormalSimilarities(Long first) {
		Map<Long, Double> map = new HashMap<>();
		for (Entry<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> e : similarities.entrySet()) {
			if (e.getKey().getElement1().equals(first)) {
				map.put(e.getKey().getElement2(), e.getValue().item1);
			} else if (e.getKey().getElement2().equals(first)) {
				map.put(e.getKey().getElement1(), e.getValue().item1);
			}
		}
		return map;
	}

	private Map<Long, Double> getEfficientSimilarities(Long first) {
		Map<Long, Double> map = new HashMap<>();

		// This should speed up the computations...
		for (Long second : totalElements) {
			SymmetricSimilarityBean<Long> s = new SymmetricSimilarityBean<Long>(first, second);
			if (second.equals(first) || similarities.get(s) == null)
				continue;

			Double sim = similarities.get(s).item1;
			if (s.getElement1().equals(first)) {
				map.put(s.getElement2(), sim);
			} else if (s.getElement2().equals(first)) {
				map.put(s.getElement1(), sim);
			}
		}
		return map;

	}

	@Override
	public void save(String path, Double confidence) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(path);

			for (SymmetricSimilarityBean<Long> sim : similarities.keySet()) {
				if (confidence.isNaN() || similarities.get(sim).item1 >= confidence)
					writer.println(sim.getElement1() + "\t" + sim.getElement2() + "\t" + similarities.get(sim).item1
							+ "\t" + similarities.get(sim).item2 + "\t" + similarities.get(sim).item3);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Error in path: " + path);
		}
	}

	@Override
	public void load(String path) {
		loadConfidence(path, Double.NaN);
	}

	@Override
	public void loadConfidence(String path, double confidenceThreshold) {
		/*
		 * Assumes that we have 3 columns. Column 0 -> first user COlumn 1 -> second
		 * user Column 2 -> similarity value in double
		 */

		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			String line = br.readLine(); // First line
			// line = br.readLine();
			while (line != null) {
				if (line != null && (!line.matches("[a-zA-Z].*"))) { // Ignore lines THAT START with letters
					String[] data = line.split("\t"); // Userid, itemID, Rating,
														// timestamp
					Long element1 = Long.parseLong(data[element1index]);
					Long element2 = Long.parseLong(data[element2index]);
					totalElements.add(element1);
					totalElements.add(element2);
					Double sim = Double.parseDouble(data[simIndex]);
					int indexEl1 = -1;
					int indexEl2 = -1;
					if (data.length > 3) { // Has the indexes
						indexEl1 = Integer.parseInt(data[simIndex + 1]);
						indexEl2 = Integer.parseInt(data[simIndex + 2]);
					}

					SymmetricSimilarityBean<Long> s = new SymmetricSimilarityBean<Long>(element1, element2);
					if (Double.isNaN(confidenceThreshold) || sim > confidenceThreshold) {
						similarities.put(s, new Tuple3D<Double, Integer, Integer>(sim, indexEl1, indexEl2));
					}
				}
				line = br.readLine();
			}
			br.close();

		} catch (FileNotFoundException e) {
			System.out.println("Error loading " + path + ". File not found");
		} catch (IOException e) {
			System.out.println("Error loading " + path + ". IOException");
		}
	}

	@Override
	public Tuple3D<Double, Integer, Integer> completeSimilarity(Long first, Long second) {
		return new Tuple3D<Double, Integer, Integer>(getSimilarity(first, second), -1, -1);
	}

	@Override
	public String info() {
		// TODO Auto-generated method stub
		return null;
	}

}
