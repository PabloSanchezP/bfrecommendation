/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.List;
import java.util.Map;

/**
 * 
 * Generic interface for a User. Each user should have: -An identifier -A map
 * matching the identifier of the items and its ratings. -A map matching the
 * identifier of the items and its timestamps. -Average rating of that user.
 *
 * In the file there is also the neighbour class that will be used to compare
 * similarity between users
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 *            type of users.
 * @param <I>
 *            type of items.
 */
public interface GenericUser<U, I> {

	/**
	 * Method to obtain the identifier of the user
	 *
	 * @return the type of the user
	 */
	public U getId();

	/**
	 * Obtain Map of the items that the user have rated
	 *
	 * @return the map with the items and its ratings
	 */
	public Map<I, Double> getItemsRated();

	/**
	 * Same map of items as before but instead of the ratings, it retrieves the
	 * timestamps
	 *
	 * @return the map with items and its timestamps
	 */
	public Map<I, Long> getItemsTimeStamp();

	/**
	 * Average of the user
	 *
	 * @return the user average
	 */
	public double getAverage();

	/**
	 * Add an item to the user
	 *
	 * @param item
	 *            the item to be added
	 * @param rating
	 *            the rating associated with that item
	 */
	public void addItem(I item, double rating);

	/**
	 * Same as before but instead of the rating, it stores the timestamp
	 *
	 * @param item
	 *            the item to be added
	 * @param timestamp
	 *            the timestamp associated with that item
	 */
	public void addItemTimeStamp(I item, Long timestamp);

	/**
	 * Add a neighbour to the user (if we use UB approach)
	 *
	 * @param user
	 *            the neighbour
	 * @param sim
	 *            the similarity associated to that neighbour
	 */
	public void addNeighbour(U user, double sim);

	/**
	 * Get all the neighbours of the user
	 *
	 * @return the list of neighbours
	 */
	public List<Neighbour<U>> getNeighbours();

	/**
	 * Get all the item preferences of the user
	 *
	 * @return the item preferences
	 */
	public List<PredictedItemPreference<I>> getItemsRecommended();

	/**
	 * Add an item to recommend to the user
	 *
	 * @param item
	 *            the item to be added
	 * @param preference
	 *            the preference associated to this item
	 */
	public void addItemRecommended(I item, double preference);

	/**
	 * Set the maximum of the neighbours to the user.
	 *
	 * @param maxNeighbours
	 *            the maximum number of neighbours
	 */
	public void setMaxNeighbours(int maxNeighbours);

	public class PredictedItemPreference<I> implements Comparable<PredictedItemPreference<I>> {

		private I item;
		private double preference;

		public PredictedItemPreference(I item, double preference) {

			this.item = item;
			this.preference = preference;
		}

		public I getIdItem() {
			return item;
		}

		public double getPreference() {
			return preference;
		}

		@Override
		public int compareTo(PredictedItemPreference<I> o) {
			if (this.preference < o.preference) {
				return -1;
			} else if (this.preference > o.preference) {
				return 1;
			} else {
				return 0;
			}
		}

		@Override
		public String toString() {
			return "[" + item + "=" + preference + "]";
		}
	}
}
