/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * * Specific class for items that are represented by ids
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class LongItem implements GenericItem<Long, Long> {

	private Long id;

	private Map<Long, Double> ratingUsers; // users and ratings
	private Map<String, List<Long>> features; // features of each item
	private double totalRatings;
	private Long totalUsersRated;

	private int maxNeighbours;
	private PriorityQueue<Neighbour<Long>> neighbours;

	// Content based
	private Map<Long, Integer> tags;

	public LongItem(Long id) {
		this.id = id;

		ratingUsers = new HashMap<>();
		totalRatings = 0;
		totalUsersRated = 0L;
		features = new HashMap<>();
		tags = new HashMap<>();
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof LongItem)) {
			return false;
		}
		LongItem item = (LongItem) other;

		return Objects.equals(item.id, this.id);
	}

	@Override
	public String toString() {
		return "HetRecItem [id=" + id + ", neighbours=" + neighbours + ", Average="
				+ totalRatings / ratingUsers.keySet().size() + "]";
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public List<Long> getFeature(String f) {
		return features.get(f);
	}

	@Override
	public double getAverage() {
		return totalRatings / ratingUsers.keySet().size();
	}

	@Override
	public void addNeighbour(Long item, double sim) {
		Neighbour<Long> toAdd = new Neighbour<>(item, sim);
		if (neighbours.size() >= maxNeighbours) {
			Neighbour<Long> lastCompare = neighbours.poll();
			if (toAdd.compareTo(lastCompare) > 0) {
				neighbours.add(toAdd);
			} else {
				neighbours.add(lastCompare);
			}
		} else {
			this.neighbours.add(toAdd);
		}
	}

	@Override
	public List<Neighbour<Long>> getNeighbours() {
		List<Neighbour<Long>> result = new ArrayList<>(neighbours);
		// Necessary to return the neighbours in the proper order
		Collections.sort(result, Collections.reverseOrder());
		return result;
	}

	@Override
	public void addUser(Long user, double rating) {
		ratingUsers.put(user, rating);
		totalRatings += rating;
	}

	@Override
	public Map<Long, Double> getUsersRated() {
		return ratingUsers;
	}

	@Override
	public void setMaxNeighbours(int maxNeighbours) {
		neighbours = new PriorityQueue<>(maxNeighbours);
		this.maxNeighbours = maxNeighbours;
	}

	public void addRating() {
		this.totalUsersRated++;
	}

	@Override
	public Long getTotalUsersRated() {
		return totalUsersRated;
	}

	@Override
	public void setFeature(String feature, List<Long> lst) {
		features.put(feature, lst);
	}

	@Override
	public Map<Long, Integer> getTags() {
		return tags;
	}

	@Override
	public void addTag(Long tag, Integer tagValue) {
		tags.put(tag, tagValue);
	}
}
