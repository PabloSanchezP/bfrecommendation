/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Generic DataModel. It Stores the users, the items and a map of features. This
 * features can be actors, directors... This features can be used to compute
 * similarities mixing Collaborative Filtering (CF) and Content Based (CB)
 * approaches.
 *
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class DataModelHetRec implements DataModelIF<Long, Long> {
	private Map<Long, GenericUser<Long, Long>> users;

	private Map<Long, GenericItem<Long, Long>> items;

	private String separator = "\t";

	private double totalRatings;
	private boolean userOriented;
	private boolean itemOriented;

	private int columnUserID = 0;
	private int columnItemID = 1;
	private int columnRate = 2;
	private int columnTimeStamp = 3;

	public DataModelHetRec(boolean userOriented, boolean itemOriented) {
		users = new TreeMap<>();
		items = new TreeMap<>();

		totalRatings = 0;

		this.userOriented = userOriented;
		this.itemOriented = itemOriented;
	}

	public void setColumnUserID(int columnUserID) {
		this.columnUserID = columnUserID;
	}

	public void setColumnItemID(int columnItemID) {
		this.columnItemID = columnItemID;
	}

	public void setColumnRate(int columnRate) {
		this.columnRate = columnRate;
	}

	public void setColumnTimeStamp(int columnTimeStamp) {
		this.columnTimeStamp = columnTimeStamp;
	}

	public DataModelHetRec() {
		this(true, true);
	}

	public void setSeparator(String separator) {
		this.separator = separator;
	}

	@Override
	public void loadFileTrain(String filepath) {
		BufferedReader br;

		totalRatings = 0;

		try {
			br = new BufferedReader(new FileReader(filepath));
			String line = br.readLine(); // First line
			while (line != null) {
				// Userid, itemID, Rating, timestamp
				String[] data = line.split(separator);
				Long itemID = Long.parseLong(data[columnItemID]);
				Long userID = Long.parseLong(data[columnUserID]);
				double itemRating = Double.parseDouble(data[columnRate]);
				long timestamp = Long.parseLong(data[columnTimeStamp]);

				addInteraction(userID, itemID, itemRating, timestamp);

				line = br.readLine();
			}

		} catch (FileNotFoundException e) {
			System.out.println("Error loading " + filepath + ". File not found");
		} catch (IOException e) {
			System.out.println("Error loading " + filepath + ". IOException");
		}

	}

	@Override
	public void addInteraction(Long userID, Long itemID, Double itemRating, Long timestamp) {
		// Items
		if (!this.items.containsKey(itemID)) {
			this.items.put(itemID, new LongItem(itemID));
		}

		// Users
		if (!this.users.containsKey(userID)) {
			this.users.put(userID, new LongUser(userID));
		}

		LongUser user = (LongUser) this.users.get(userID);
		LongItem item = (LongItem) this.items.get(itemID);

		this.totalRatings++;

		if (userOriented) {
			// Insert that item to the user
			user.addItem(itemID, itemRating);
			// Insert that timestamp
			user.addItemTimeStamp(itemID, timestamp);
		}

		if (itemOriented) {
			// Insert the user to the item
			item.addUser(userID, itemRating);
		}

		item.addRating();
	}

	@Override
	public Map<Long, GenericUser<Long, Long>> getUsers() {
		return this.users;
	}

	@Override
	public Map<Long, GenericItem<Long, Long>> getItems() {
		return this.items;
	}

	@Override
	public double getTotalRatings() {
		return totalRatings;
	}


	@Override
	public void addItem(Long item) {
		this.items.put(item, new LongItem(item));
	}

	@Override
	public void addUser(Long user) {
		this.users.put(user, new LongUser(user));
	}




	@Override
	public void describe() {
		System.out.println("Number of users: " + users.size());
		System.out.println("Number of items: " + items.size());
		System.out.println("Avg of user consumed a ratings: " + (double) users.size() / items.size());
	}

	@Override
	public boolean isItemBased() {
		return itemOriented;
	}

	@Override
	public boolean isUserBased() {
		return userOriented;
	}

}
