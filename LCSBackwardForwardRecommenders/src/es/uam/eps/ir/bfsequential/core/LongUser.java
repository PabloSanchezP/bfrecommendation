/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * Specific class for users that are represented by longs
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public class LongUser implements GenericUser<Long, Long> {

	private Long id;
	private Map<Long, Double> itemsRated; // items and ratings
	private Map<Long, Long> itemsTimeStamp; // items and timestamps

	private PriorityQueue<Neighbour<Long>> neighbours;
	private double totalRatings;

	private int maxNeighbours;

	private List<PredictedItemPreference<Long>> itemsRecommended;

	public LongUser(long id) {
		this.id = id;
		itemsRated = new HashMap<>();
		itemsTimeStamp = new HashMap<>();
		totalRatings = 0;
		itemsRecommended = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "HetRecUser [id=" + id + ", neighbours=" + neighbours + ", Average="
				+ totalRatings / itemsRated.keySet().size() + "]";
	}

	@Override
	public Long getId() {
		return this.id;
	}

	@Override
	public Map<Long, Double> getItemsRated() {
		return itemsRated;
	}

	@Override
	public double getAverage() {
		return totalRatings / itemsRated.keySet().size();
	}

	@Override
	public void addItem(Long item, double rating) {
		itemsRated.put(item, rating);
		totalRatings += rating;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other == this) {
			return true;
		}
		if (!(other instanceof LongUser)) {
			return false;
		}
		LongUser user = (LongUser) other;

		return Objects.equals(user.id, this.id);

	}

	@Override
	public Map<Long, Long> getItemsTimeStamp() {
		return itemsTimeStamp;
	}

	@Override
	public void addItemTimeStamp(Long item, Long timestamp) {
		itemsTimeStamp.put(item, timestamp);

	}

	@Override
	public List<Neighbour<Long>> getNeighbours() {
		List<Neighbour<Long>> result = new ArrayList<>(neighbours);
		// Necessary to return the neighbours in the proper order
		Collections.sort(result, Collections.reverseOrder());
		return result;
	}

	@Override
	public void addNeighbour(Long user, double sim) {
		Neighbour<Long> toAdd = new Neighbour<>(user, sim);
		if (neighbours.size() >= maxNeighbours) {
			Neighbour<Long> lastCompare = neighbours.poll();
			if (toAdd.compareTo(lastCompare) > 0) {
				neighbours.add(toAdd);
			} else {
				neighbours.add(lastCompare);
			}
		} else {
			this.neighbours.add(toAdd);
		}
	}

	@Override
	public List<PredictedItemPreference<Long>> getItemsRecommended() {
		return itemsRecommended;
	}

	@Override
	public void addItemRecommended(Long item, double preference) {
		itemsRecommended.add(new PredictedItemPreference<>(item, preference));
	}

	@Override
	public void setMaxNeighbours(int maxNeighbours) {
		neighbours = new PriorityQueue<>(maxNeighbours);
		this.maxNeighbours = maxNeighbours;
	}

}
