/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.List;
import java.util.Map;

/**
 *
 * Generic interface for an Item. Each item should have: -An identifier -Average
 * rating of that item. -A set of features of the items (directors, actors,
 * groups, countries) -
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 *            the type of users
 * @param <I>
 *            the type of items
 *
 */
public interface GenericItem<U, I> {

	public I getId();

	/**
	 * * Some items can store tags to store contentBased information
	 *
	 * @return
	 */
	public Map<Long, Integer> getTags();

	/**
	 * * Add a tag to the items list tag
	 *
	 * @param tag
	 * @param tagValue
	 */
	public void addTag(Long tag, Integer tagValue);

	/**
	 * Some items can store information about its features
	 *
	 * @param f
	 *            the feature
	 * @return the list of ids of that feature
	 */
	public List<Long> getFeature(String f);

	/**
	 * We can set an array of feature identifiers
	 *
	 * @param feature
	 *            the feature
	 * @param lst
	 *            the list of longs
	 */
	public void setFeature(String feature, List<Long> lst);

	/**
	 * Method to obtain the average of ratings of the item.
	 *
	 * @return
	 */
	public double getAverage();

	/**
	 * Set maximum neighbours to use in the item-based approach
	 *
	 * @param maxNeighbours
	 */
	public void setMaxNeighbours(int maxNeighbours);

	/**
	 * Method to add a user to that item. A item can be rated by many users
	 *
	 * @param user
	 * @param rating
	 */
	public void addUser(U user, double rating);

	/**
	 * Obtain a Map of all users that have rated this item
	 *
	 * @return
	 */
	public Map<U, Double> getUsersRated();

	/**
	 * Method to add a neighbour (another item) to this one
	 *
	 * @param item
	 *            the item to add
	 * @param sim
	 *            the similarity of the new neighbor
	 */
	public void addNeighbour(I item, double sim);

	/**
	 * Method to get the neighbours of the item
	 *
	 * @return a list of neighbours
	 */
	public List<Neighbour<I>> getNeighbours();

	/**
	 * Method to obtain all the users that have rated the item
	 *
	 * @return
	 */
	public Long getTotalUsersRated();

}
