/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.List;

import es.uam.eps.ir.bfsequential.core.GenericUser.PredictedItemPreference;

/**
 *
 * Recommender Interface.
 *
 * Each Recommender should have a DataModel inside.
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U>
 *            The type of the users.
 * @param <I>
 *            The type of the items.
 */
public interface RecommenderIF<U, I> {

	/**
	 * Train method
	 */
	public void train();

	/**
	 * Predict interaction method. Receiving and user and a item, it will predict
	 * what rating will assign that user to the item
	 *
	 * @param user
	 *            the user
	 * @param item
	 *            the item.
	 * @return a double indicating the value
	 */
	public double predictInteraction(U user, I item);

	/**
	 * Method to obtain a list of recommended items to the user
	 *
	 * @param user
	 * @param numberItems
	 * @return a list of item preferences.
	 */
	public List<PredictedItemPreference<Long>> recommend(U user, int numberItems);

	/**
	 * Method to write the ranking to a file
	 *
	 * @param path
	 * @param numberItems
	 * @param dataModel
	 */
	public void writeRanking(String path, int numberItems, DataModelIF<U, I> dataModel);
}
