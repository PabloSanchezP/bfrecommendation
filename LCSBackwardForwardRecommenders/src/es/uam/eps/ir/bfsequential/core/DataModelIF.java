/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.core;

import java.util.Map;

/**
 * Interface of DataModel. Important methods are loading ones and Getting Users
 * and items. It also has a method to add a neighbour.
 *
 * @author Pablo Sanchez (pablo.sanchezp@estudiante.uam.es)
 *
 * @param <U>
 *            user type
 * @param <I>
 *            item type
 */
public interface DataModelIF<U, I> {

	/***
	 * Method to load the train file
	 * @param filepath
	 */
	public void loadFileTrain(String filepath);

	/**
	 * Obtain the total number of ratings of the datamodel
	 * @return
	 */
	public double getTotalRatings();

	/**
	 * Method to get all the users in the system and all the items rated by the users
	 * @return
	 */
	public Map<U, GenericUser<U, I>> getUsers();

	/**
	 * Method to get all the items in the system and all the users that have rated that item
	 * @return
	 */
	public Map<I, GenericItem<U, I>> getItems();

	/***
	 * Method to insert an item in the system
	 * @param item the item
	 */
	public void addItem(I item);

	/***
	 * Method to insert a user in the system
	 * @param user
	 */
	public void addUser(U user);

	/**
	 * Method to add an interaction between a user and an item in the system
	 * @param user the user
	 * @param item the item
	 * @param r the rating
	 * @param t the timestamp
	 */
	public void addInteraction(U user, I item, Double r, Long t);

	/**
	 * Method to check if the datamodel is user based
	 * @return
	 */
	public boolean isUserBased();
	
	/**
	 * Method to check if the datamodel is item based
	 * @return
	 */
	public boolean isItemBased();

	/***
	 * Method to describe the datamodel
	 */
	public void describe();

}
