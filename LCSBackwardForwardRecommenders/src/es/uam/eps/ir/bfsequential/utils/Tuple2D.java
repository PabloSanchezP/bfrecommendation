/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.utils;

/**
 * Tuples with 2 items
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * 
 * @param <T1>
 *            type of first parameter
 * @param <T2>
 *            type of second parameter
 */
public class Tuple2D<T1, T2> {

	public final T1 item1;
	public final T2 item2;

	public Tuple2D(final T1 item_1, final T2 item_2) {
		item1 = item_1;
		item2 = item_2;
	}

	public int size() {
		return 2;
	}
}
