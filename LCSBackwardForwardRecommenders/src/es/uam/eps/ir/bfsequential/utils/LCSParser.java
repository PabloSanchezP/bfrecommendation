/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.utils;

import java.util.Arrays;

import java.util.HashMap;

import java.util.Map;
import java.util.TreeMap;

import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.core.GenericUser;

/**
 * LCSParser class. It determines the transformation of the users/items
 * depending on the training model.
 * 
 * 
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class LCSParser {

	// Possible orders
	private static final String ORDEREDBY_ID = "ORDEREDBY_ID";
	private static final String ORDEREDBY_TIMESTAMP = "ORDEREDBY_TIMESTAMP";
	private static final String ORDEREDBY_RATING = "ORDEREDBY_RATING";

	// Possible substitutions of IDs
	private static final String ITEMIDRATING = "ITEMIDRATING";
	private static final String ITEMID = "ITEMID";
	private static final String ITEMRATING = "ITEMRATING";

	// All models must be done using the final strings of above.
	public enum UBTrainModel {
		// ORDER_ID
		ITEMIDRATING_ORDEREDBY_ID, ITEMID_ORDEREDBY_ID, ITEMRATING_ORDEREDBY_ID,

		// ORDER_TIMESTAMP
		ITEMIDRATING_ORDEREDBY_TIMESTAMP, ITEMID_ORDEREDBY_TIMESTAMP, ITEMRATING_ORDEREDBY_TIMESTAMP,
	};

	public static Map<Long, Integer[]> userToLCSTransformation(DataModelIF<Long, Long> model, UBTrainModel m,
			int idsFactor, int Ratingfactor) {

		switch (m) {
		// All this CF
		case ITEMIDRATING_ORDEREDBY_ID:
		case ITEMID_ORDEREDBY_ID:
		case ITEMRATING_ORDEREDBY_ID:
			// Orderings
		case ITEMID_ORDEREDBY_TIMESTAMP:
		case ITEMIDRATING_ORDEREDBY_TIMESTAMP:
		case ITEMRATING_ORDEREDBY_TIMESTAMP:
			return userToLCSItemIDOrderedBySm(model, m, idsFactor, Ratingfactor); // Ordered by timestamp or by id
		default:
			return null;
		}

	}

	/**
	 * Transformation using only id of items. For all users we will obtain the items
	 * that he have rated and add them in an transformed array. The size of the
	 * result array will be the same of the total items rated by the user
	 * 
	 * @param model
	 * @param m
	 * @param p
	 * @param ratTrans
	 * @param MFACTORID
	 * @param MFACTORRATING
	 * @return
	 */
	private static Map<Long, Integer[]> userToLCSItemIDOrderedBySm(DataModelIF<Long, Long> model, UBTrainModel m,
			int MFACTORID, int MFACTORRATING) {
		Map<Long, Integer[]> transformation = new HashMap<Long, Integer[]>();
		if (!model.isUserBased()) {
			throw new IllegalArgumentException("Model does not support users!");
		}

		// For every user
		for (Long user : model.getUsers().keySet()) {
			// Get the generic user
			GenericUser<Long, Long> u = model.getUsers().get(user);
			Integer[] arrayTransform = new Integer[u.getItemsRated().keySet().size()];

			Map<Long, Long> result = obtainMapOrdering(m, u, model);

			int i = 0;
			// We obtain the items of the user ordered
			for (Long idItem : result.keySet()) {
				double r = u.getItemsRated().get(idItem);
				if (modelHasString(m, ITEMIDRATING)) {
					// IDItem + rating: each element of the array will be the key *100+rate *10
					arrayTransform[i] = (int) (idItem * MFACTORID + (int) (r * MFACTORRATING));
				} else if (modelHasString(m, ITEMID)) {
					arrayTransform[i] = (int) (idItem.longValue() * MFACTORID);
				} else if (modelHasString(m, ITEMRATING)) {
					arrayTransform[i] = (int) (r * MFACTORRATING);
				} else {
					continue;
				}
				i++;
			}
			transformation.put(user, Arrays.copyOfRange(arrayTransform, 0, i));
		}
		return transformation;
	}

	/****
	 * Method to obtain the ordering 
	 * @param tm
	 * @param u
	 * @param model
	 * @return
	 */
	private static <K extends Comparable<K>, V extends Comparable<V>> Map<K, V> obtainMapOrdering(UBTrainModel tm,
			GenericUser<Long, Long> u, DataModelIF<Long, Long> model) {
		if (modelHasString(tm, ORDEREDBY_ID)) {
			return (Map<K, V>) new TreeMap<Long, Double>(u.getItemsRated());
		} else if (modelHasString(tm, ORDEREDBY_TIMESTAMP)) {
			return (Map<K, V>) RecommendationUtils.sortByValue(u.getItemsTimeStamp(), false);
		} else if (modelHasString(tm, ORDEREDBY_RATING)) {
			return (Map<K, V>) RecommendationUtils.sortByValue(u.getItemsRated(), true);
		} else
			return null;
	}

	/***
	 * Method to check if a specific model contains a string. Useful for
	 * different ways to order the transformation of the users.
	 * 
	 * @param model the user training model
	 * @param match the string to match
	 * @return true of there is a matching, false otherwise
	 * 
	 */
	public static boolean modelHasString(UBTrainModel model, String match) {
		if (model.name().contains(match) || model.name().toLowerCase().contains(match)) {
			return true;
		}
		return false;

	}

}
