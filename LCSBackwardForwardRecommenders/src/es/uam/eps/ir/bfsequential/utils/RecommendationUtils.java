/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple3;
import org.ranksys.core.util.tuples.Tuple2od;
import org.ranksys.lda.LDAModelEstimator;
import org.ranksys.lda.LDARecommender;

import cc.mallet.topics.ParallelTopicModel;
import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.recommenders.RankSysTemporalRecommender;
import es.uam.eps.ir.bfsequential.sim.UBSimilarityRankSys;
import es.uam.eps.ir.bfsequential.utils.RecommendationUtils.Preference;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.mf.Factorization;
import es.uam.eps.ir.ranksys.mf.als.HKVFactorizer;
import es.uam.eps.ir.ranksys.mf.als.PZTFactorizer;
import es.uam.eps.ir.ranksys.mf.plsa.PLSAFactorizer;
import es.uam.eps.ir.ranksys.mf.rec.MFRecommender;
import es.uam.eps.ir.ranksys.nn.item.ItemNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.CachedItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.ItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.neighborhood.TopKItemNeighborhood;
import es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.SetJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorCosineItemSimilarity;
import es.uam.eps.ir.ranksys.nn.item.sim.VectorJaccardItemSimilarity;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.sim.SetCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.SetJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorCosineUserSimilarity;
import es.uam.eps.ir.ranksys.nn.user.sim.VectorJaccardUserSimilarity;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.RandomRecommender;

/**
 * Final class that contains useful methods in order to work in a more general
 * way.
 *
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 */
public final class RecommendationUtils {

	
	public static <K extends Comparable<K>, V extends Comparable<V>> Map<K, V> sortByValue(Map<K, V> unsortMap,
			boolean reverse) {

		// 1. Convert Map to List of Map
		List<Map.Entry<K, V>> list = new LinkedList<>(unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		// Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				if (reverse) {
					return (o2.getValue()).compareTo(o1.getValue());
				} else {
					return (o1.getValue()).compareTo(o2.getValue());
				}
			}
		});

		// 3. Loop the sorted list and put it into a new insertion order Map
		// LinkedHashMap
		Map<K, V> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/***
	 * Method to obtain a rankSys recommender from the parameters of the main
	 * @param rec the recommender
	 * @param similarity the rankSys similarity
	 * @param data the preference data
	 * @param kNeighbours the neighbours
	 * @param userIndex the user index
	 * @param itemIndex the item index
	 * @param kFactorizer the factorizer of the MF
	 * @param alphaF the alpha for the HKV
	 * @param lambdaF the lambda value
	 * @param numInteractions the number of interactions
	 * @param lc4RecsysDM the datamodel (our datamodel)
	 * @param lambdaTemporal the temporal lambda for our temporal recommender
	 * @return
	 */
	public static Recommender<Long, Long> obtRankSysRecommeder(String rec, String similarity,
			FastPreferenceData<Long, Long> data, int kNeighbours, FastUserIndex<Long> userIndex,
			FastItemIndex<Long> itemIndex, int kFactorizer, double alphaF, double lambdaF, int numInteractions,
			DataModelIF<Long, Long> lc4RecsysDM, double lambdaTemporal, String simFile) {
		switch (rec) {
		case "RandomRecommender":
        	System.out.println("RandomRecommender");
			return new RandomRecommender<>(data, data);
		case "PopularityRecommender":
        	System.out.println("PopularityRecommender");
			return new PopularityRecommender<>(data);

		case "UserNeighborhoodRecommender": // User based
			
			System.out.println("UserNeighborhoodRecommender");
        	System.out.println("kNeighs: "+kNeighbours);
        	System.out.println("Sim: "+similarity);
        	
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNR = obtRanksysUserSimilarity(data, similarity);
			if (simUNR == null)
				return null;
			UserNeighborhood<Long> urneighborhood = new TopKUserNeighborhood<>(simUNR, kNeighbours);
			return new UserNeighborhoodRecommender<>(data, urneighborhood, 1);
		case "ItemNeighborhoodRecommender": // Item based
			
			System.out.println("ItemNeighborhoodRecommender");
        	System.out.println("kNeighs: "+kNeighbours);
        	System.out.println("Sim: "+similarity);
        	
			es.uam.eps.ir.ranksys.nn.item.sim.ItemSimilarity<Long> simINR = obtRanksysItemSimilarity(data, similarity);
			ItemNeighborhood<Long> neighborhood = new TopKItemNeighborhood<>(simINR, kNeighbours);
			neighborhood = new CachedItemNeighborhood<>(neighborhood);
			return new ItemNeighborhoodRecommender<>(data, neighborhood, 1);

		case "RankSysTemporalRecommender": { // Temporal recommender
			
			System.out.println("RankSysTemporalRecommender");
        	System.out.println("Temporal lambda: " + lambdaTemporal);
			
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simUNRT = obtRanksysUserSimilarity(data, similarity);
			if (simUNRT == null)
				return null;
			UserNeighborhood<Long> urneighborhoodT = new TopKUserNeighborhood<>(simUNRT, kNeighbours);
			return new RankSysTemporalRecommender<>(data, urneighborhoodT, 1, lc4RecsysDM, lambdaTemporal);
		}
		
		case "RankSysTemporalRecommenderRead": { // Temporal recommender
			
			System.out.println("RankSysTemporalRecommender");
        	System.out.println("Temporal lambda: " + lambdaTemporal);
			
        	es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simRankSys = new UBSimilarityRankSys(
        			data, simFile, Double.NaN);
        	
			UserNeighborhood<Long> urneighborhoodT = new TopKUserNeighborhood<>(simRankSys, kNeighbours);
			return new RankSysTemporalRecommender<>(data, urneighborhoodT, 1, lc4RecsysDM, lambdaTemporal);
		}
		
		
		case "MFRecommenderHKV": { // Matrix factorization. Y. Hu, Y. Koren, C. Volinsky. Collaborative filtering for implicit feedback datasets. ICDM 2008.
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			
			System.out.println("MFRecommenderHKV");
            System.out.println("kFactors: "+ k);
            System.out.println("lambda: "+ lambda);
            System.out.println("alpha: "+ alpha);
            System.out.println("numIter: "+ numIter);

			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new HKVFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "MFRecommenderPZT": { // I. Pilászy, D. Zibriczky and D. Tikk. Fast ALS-based Matrix Factorization for Explicit and Implicit Feedback Datasets. RecSys 2010
			int k = kFactorizer;
			double lambda = lambdaF;
			double alpha = alphaF;
			int numIter = numInteractions;
			
			System.out.println("MFRecommenderPZT");
            System.out.println("kFactors: "+ k);
            System.out.println("lambda: "+ lambda);
            System.out.println("alpha: "+ alpha);
            System.out.println("numIter: "+ numIter);
			
			DoubleUnaryOperator confidence = x -> 1 + alpha * x;
			Factorization<Long, Long> factorization = new PZTFactorizer<Long, Long>(lambda, confidence, numIter)
					.factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "MFRecommenderPLSA": { // T. Hofmann. Latent Semantic Models for Collaborative Filtering. ToIS, Vol 22  No. 1, January 2004.
			int k = kFactorizer;
			int numIter = numInteractions;
			
			System.out.println("MFRecommenderPLSA");
            System.out.println("kFactors: "+ k);
            System.out.println("numIter: "+ numIter);
			
			Factorization<Long, Long> factorization = new PLSAFactorizer<Long, Long>(numIter).factorize(k, data);
			return new MFRecommender<>(userIndex, itemIndex, factorization);
		}
		case "LDARecommender": {
			int k = kFactorizer;
			double alpha = alphaF;
			int numIter = numInteractions;
			double beta = 0.01;
			int burninPeriod = 50;
			
			 System.out.println("LDARecommender");
             System.out.println("kFactors: "+ k);
             System.out.println("numIter: "+ numIter);
             System.out.println("beta: "+ beta);
             System.out.println("burninPeriod: "+ burninPeriod);
             
			ParallelTopicModel topicModel = null;
			try {
				topicModel = LDAModelEstimator.estimate(data, k, alpha, beta, numIter, burninPeriod);
			} catch (IOException e) {
				return null;
			}
			return new LDARecommender<>(userIndex, itemIndex, topicModel);
		}

		default:
			return null;
		}

	}

	/**
	 * Obtain RankSys User Similarity
	 *
	 * @param data
	 * @param similarity
	 * @return
	 */
	public static UserSimilarity<Long> obtRanksysUserSimilarity(FastPreferenceData<Long, Long> data,
			String similarity) {
		switch (similarity) {
		case "VectorCosineUserSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineUserSimilarity<>(data, 0.5, true);
		case "VectorJaccardUserSimilarity":
			return new VectorJaccardUserSimilarity<>(data, true);
		case "SetJaccardUserSimilarity":
			return new SetJaccardUserSimilarity<>(data, true);
		case "SetCosineUserSimilarity":
			return new SetCosineUserSimilarity<>(data, 0.5, true);
		default:
			return null;
		}
	}

	/**
	 * Obtain RankSys Item Similarity
	 *
	 * @param data
	 * @param similarity
	 * @return
	 */
	private static ItemSimilarity<Long> obtRanksysItemSimilarity(FastPreferenceData<Long, Long> data,
			String similarity) {
		switch (similarity) {
		case "VectorCosineItemSimilarity":
			// 0.5 to make it symmetrical.
			return new VectorCosineItemSimilarity<>(data, 0.5, true);
		case "VectorJaccardItemSimilarity":
			return new VectorJaccardItemSimilarity<>(data, true);
		case "SetJaccardItemSimilarity":
			return new SetJaccardItemSimilarity<>(data, true);
		default:
			return null;
		}
	}

	/**
	 * * Method to store a ranking file of a rankSys recommender
	 *
	 * @param ranksysTrainData
	 * @param ranksystestData
	 * @param rankSysrec
	 * @param outputFile
	 * @param numberItemsRecommend
	 */
	public static void ranksysWriteRanking(FastPreferenceData<Long, Long> ranksysTrainData,
			FastPreferenceData<Long, Long> ranksystestData, Recommender<Long, Long> rankSysrec, String outputFile,
			int numberItemsRecommend) {
		PrintStream out;
		try {
			out = new PrintStream(outputFile);
			Stream<Long> targetUsers = ranksystestData.getAllUsers();
			System.out.println("Users in test data " + ranksystestData.numUsers());
			targetUsers.forEach(user -> {
				if (ranksystestData.getUserPreferences(user).count() != 0L && (ranksysTrainData.containsUser(user))) {
					int rank = 1;
					Recommendation<Long, Long> rec = rankSysrec.getRecommendation(user, numberItemsRecommend,
							TrainItems(user, ranksysTrainData));
					for (Tuple2od<Long> tup : rec.getItems()) { // Items recommended
						// We should see if the items are in train
						out.println(formatRank(user, tup.v1, tup.v2, rank));
						rank++;
					}
				}
			});
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * Method to obtain the best indexes in a equivalent way than the LCS algorithm
	 * 
	 * @param timeStampOrderedU1
	 *            the list of the first user
	 * @param timeStampOrderedU2
	 *            the list of the second user
	 * @return the tuple of last indexes
	 */
	public static Tuple2D<Integer, Integer> obtainBestIndexesLCSEquivalent(List<Long> timeStampOrderedU1,
			List<Long> timeStampOrderedU2) {
		int indexU1 = -1;
		int indexU2 = -1;

		Set<Long> timeStampOrderedU1Set = new HashSet<>(timeStampOrderedU1);
		Set<Long> timeStampOrderedU2Set = new HashSet<>(timeStampOrderedU2);

		// Obtain the index of the first user
		for (int i = timeStampOrderedU1.size() - 1; i >= 0; i--) {
			Long itemU1 = timeStampOrderedU1.get(i);

			if (timeStampOrderedU2Set.contains(itemU1)) {
				indexU1 = i;
				break;
			}

		}

		// Obtain the index of the second user
		for (int i = timeStampOrderedU2.size() - 1; i >= 0; i--) {
			Long itemU2 = timeStampOrderedU2.get(i);
			if (timeStampOrderedU1Set.contains(itemU2)) {
				indexU2 = i;
				break;
			}

		}
		return new Tuple2D<Integer, Integer>(indexU1, indexU2);

	}

	/***
	 * Predicate that will return true if the item has been rated in the training
	 * set
	 * 
	 * @param trainData
	 *            the trainData
	 * @return
	 */
	public static <U, I> Predicate<I> itemWithRatingInTrain(PreferenceData<U, I> trainData) {
		return p -> trainData.getItemPreferences(p).findFirst().isPresent();
	}

	/***
	 * Train items strategy of the predicate. -Only items with at least one rating
	 * in train should be recommended. -Only items that the user have not rated in
	 * train (new items for the user).
	 * 
	 * @param user
	 *            the user
	 * @param trainData
	 *            the trainData
	 * @return
	 */
	public static <U, I> Predicate<I> TrainItems(U user, PreferenceData<U, I> trainData) {
		return isNotInTrain(user, trainData).and(itemWithRatingInTrain(trainData));
	}

	/**
	 * Predicate used in ranksys to make recommendations not to be in train
	 * 
	 * @param user
	 *            the user
	 * @param data
	 *            the RanksysFastDatamodel
	 * @return A predicate
	 */
	public static <U, I> Predicate<I> isNotInTrain(U user, PreferenceData<U, I> data) {
		return p -> data.getUserPreferences(user).noneMatch(itemPref -> itemPref.v1.equals(p));

	}

	/***
	 * Method print the recommendation score for an item
	 * 
	 * @param user
	 *            the user
	 * @param item
	 *            the item
	 * @param valueItem
	 *            the score provided by the recommender
	 * @param rank
	 *            the rank
	 * @return a string with all the filds separated by tabulations
	 */
	public static String formatRank(Long user, Long item, double valueItem, int rank) {
		return user + "\t" + item + "\t" + valueItem + "\t" + rank;
	}

	/***
	 * Method to compute the average between different result files
	 * 
	 * @param dirPath
	 *            the path where the different files to average are
	 * @param filePattern
	 *            the patter that all files to average must match
	 * @param resFile
	 *            the result file
	 */
	public static void averageResults(String dirPath, String filePattern, String resFile) {

		try {
			File dir = new File(dirPath);
			File[] files = dir.listFiles((d, name) -> name.contains(filePattern) && !name.contains("AVERAGE"));
			if (files == null || files.length == 0) {
				return;
			}
			double divide = files.length;
			Map<String, Double> map = new LinkedHashMap<String, Double>();
			for (File file : files) {
				System.out.println(file.getPath());
				Stream<String> stream = Files.lines(file.toPath());
				stream.forEach(line -> {
					String[] fullData = line.split("\t");
					if (map.get(fullData[0]) == null)
						map.put(fullData[0], Double.parseDouble(fullData[1]));
					else
						map.put(fullData[0], map.get(fullData[0]) + Double.parseDouble(fullData[1]));

				});
				stream.close();
			}
			PrintStream resultFile = new PrintStream(resFile);
			for (String metric : map.keySet()) {
				resultFile.println(metric + "\t" + (map.get(metric) / divide));
			}
			resultFile.close();
			System.out.println("Writed on " + resFile);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/***
	 * Method to obtain the minimum timestamp of an specific item
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @param itemId
	 *            the item identifier
	 * @return the minimum timestamp (the first timestamp in which a user rated the
	 *         item)
	 */
	public static double getMinTimeStampOfItem(DataModelIF<Long, Long> dataModel, Long itemId) {
		long res = Long.MAX_VALUE;
		for (Long user : dataModel.getUsers().keySet()) {
			if (dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId) != null
					&& res > dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId)) {
				res = dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId);
			}
		}
		return res;
	}

	/***
	 * Method to obtain the maximum timestamp of an specific item
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @param itemId
	 *            the item identifier
	 * @return the maximum timestamp (the last timestamp in which a user rated the
	 *         item)
	 */
	public static double getMaxTimeStampOfItem(DataModelIF<Long, Long> dataModel, Long itemId) {
		long res = 0;
		for (Long user : dataModel.getUsers().keySet()) {
			if (dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId) != null
					&& res < dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId)) {
				res = dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId);
			}
		}
		return res;
	}

	/***
	 * Method to obtain the maximum timestamp of a datamodel
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @return the maximum timestamp
	 */
	public static double getLargestTimeStamp(DataModelIF<Long, Long> dataModel) {
		double maxTime = -1;
		for (Long user : dataModel.getUsers().keySet()) {
			long maxOfUser = dataModel.getUsers().get(user).getItemsTimeStamp().values().stream()
					.mapToLong(Long::longValue).max().getAsLong();
			if (maxTime < maxOfUser) {
				maxTime = maxOfUser;
			}
		}
		return maxTime;
	}

	/***
	 * Method to obtain the minimum timestamp of a datamodel
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @return the minimum timestamp
	 */
	public static double getMinimumTimeStamp(DataModelIF<Long, Long> dataModel) {
		double minTime = Double.MAX_VALUE;
		for (Long user : dataModel.getUsers().keySet()) {
			long minOfUser = dataModel.getUsers().get(user).getItemsTimeStamp().values().stream()
					.mapToLong(Long::longValue).min().getAsLong();
			if (minTime > minOfUser) {
				minTime = minOfUser;
			}
		}
		return minTime;
	}

	/***
	 * Method to obtain the average timestamp of an specific item
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @param itemId
	 *            the item identifier
	 * @return the average of timestamps of the ratings of an item
	 */
	public static double getAverageTimeStampOfItem(DataModelIF<Long, Long> dataModel, Long itemId) {
		long acc = 0;
		long count = 0;
		for (Long user : dataModel.getUsers().keySet()) {
			if (dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId) != null) {
				acc += dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId);
				count += 1;
			}
		}
		return (double) acc / (double) count;
	}

	/***
	 * Method to obtain the median timestamp of an specific item
	 * 
	 * @param dataModel
	 *            the datamodel
	 * @param itemId
	 *            the item identifier
	 * @return the median of timestamps of the ratings of an item
	 */
	public static double getMedianTimeStampOfItem(DataModelIF<Long, Long> dataModel, Long itemId) {
		List<Long> storation = new ArrayList<Long>();
		for (Long user : dataModel.getUsers().keySet()) {
			if (dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId) != null) {
				storation.add(dataModel.getUsers().get(user).getItemsTimeStamp().get(itemId));
			}
		}
		Long[] toArr = new Long[storation.size()];
		toArr = storation.toArray(toArr);
		Arrays.sort(toArr);
		if (toArr.length % 2 == 0) {
			return ((double) toArr[toArr.length / 2] + (double) toArr[toArr.length / 2 - 1]) / 2.0;
		} else {
			return toArr[toArr.length / 2];
		}

	}
	
	/***
	 * Method to reduce the number of ratings of a dataset
	 * 
	 * @param srcPath
	 *            the source file
	 * @param dstPath
	 *            the destination file
	 * @param numberOfRatingsUser
	 *            the minimum number of ratings that the user needs to have
	 * @param numberOfRatingsItems
	 *            the minimum number of ratings that the item needs to have
	 */
	public static void DatasetReductionRatings(String srcPath, String dstPath, int numberOfRatingsUser,
			int numberOfRatingsItems) {
		// Assume structure of the file is: UserdId(long) ItemID(long)
		// rating(double) timestamp(long)

		// Map of users -> userID and list of ItemID-rating-timestamp
		Map<Long, List<Tuple3<Long, Double, Long>>> users = new TreeMap<Long, List<Tuple3<Long, Double, Long>>>();
		// Map of items -> itemID and list of UserID-rating-timestamp
		Map<Long, List<Tuple3<Long, Double, Long>>> items = new TreeMap<Long, List<Tuple3<Long, Double, Long>>>();

		String characterSplit = "\t";

		// Parameters to configure
		int columnUser = 0;
		int columnItem = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;

		PrintStream writer = null;

		try (Stream<String> stream = Files.lines(Paths.get(srcPath))) {
			stream.forEach(line -> {
				String[] data = line.split(characterSplit);
				Long idUser = Long.parseLong(data[columnUser]);
				Long idItem = Long.parseLong(data[columnItem]);
				double rating = Double.parseDouble((data[columnRating]));

				Long timestamp = Long.parseLong(data[columnTimeStamp]);

				List<Tuple3<Long, Double, Long>> lstu = users.get(idUser);
				if (lstu == null) { // New user
					lstu = new ArrayList<>();
					// Add item to list
					users.put(idUser, lstu);
				}
				lstu.add(new Tuple3<Long, Double, Long>(idItem, rating, timestamp));

				List<Tuple3<Long, Double, Long>> lsti = items.get(idItem);
				if (lsti == null) { // New item
					lsti = new ArrayList<>();
					// Add item to list
					items.put(idItem, lsti);
				}
				lsti.add(new Tuple3<Long, Double, Long>(idUser, rating, timestamp));
			});
			stream.close();

			while (true) {
				if (checkStateCore(users, items, numberOfRatingsUser, numberOfRatingsItems)) {
					break;
				}
				updateMaps(users, items, numberOfRatingsUser, numberOfRatingsItems);
			}

			writer = new PrintStream(dstPath);
			// Writing the new data to the file
			for (Long user : users.keySet()) {
				List<Tuple3<Long, Double, Long>> lst = users.get(user);
				for (Tuple3<Long, Double, Long> t : lst) {
					writer.println(user + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3);
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/***
	 * Method to update the maps of the users/items
	 * 
	 * @param users
	 *            the users and their corresponding preferences
	 * @param items
	 *            the items and their corresponding preferences
	 * @param minimumRatingUsers
	 *            the minimum number of ratings required per every user
	 * @param minimumRatingItems
	 *            the minimum number of ratings required per every item
	 * @return true if the condition is satisfied, false otherwise
	 */
	private static void updateMaps(Map<Long, List<Tuple3<Long, Double, Long>>> users,
			Map<Long, List<Tuple3<Long, Double, Long>>> items, int minimumRatingUsers, int minimumRatingItems) {
		Set<Long> usersWithLess = new TreeSet<>(
				users.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingUsers).map(t -> t.getKey())
						.collect(Collectors.toSet()));
		Set<Long> itemsWithLess = new TreeSet<>(
				items.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingItems).map(t -> t.getKey())
						.collect(Collectors.toSet()));

		// Now for the items we must remove the users that have rated that item
		// AND the items that have less than that number of ratings
		Set<Long> totalItems = new TreeSet<>(items.keySet());
		for (Long item : totalItems) {
			List<Tuple3<Long, Double, Long>> lst = items.get(item);
			items.put(item, new ArrayList<>(
					lst.stream().filter(t -> !usersWithLess.contains(t.v1)).collect(Collectors.toList())));
		}

		// Now remove the items from the users
		Set<Long> totalUsers = new TreeSet<>(users.keySet());
		for (Long user : totalUsers) {
			List<Tuple3<Long, Double, Long>> lst = users.get(user);
			users.put(user, new ArrayList<>(
					lst.stream().filter(t -> !itemsWithLess.contains(t.v1)).collect(Collectors.toList())));
		}

		// Now remove all items and all users that have less than the ratings
		Set<Long> it = new TreeSet<>(users.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingUsers)
				.map(t -> t.getKey()).collect(Collectors.toSet()));
		for (Long user : it)
			users.remove(user);

		it = new TreeSet<>(items.entrySet().stream().filter(t -> t.getValue().size() < minimumRatingItems)
				.map(t -> t.getKey()).collect(Collectors.toSet()));
		for (Long item : it)
			items.remove(item);

		//
		totalUsers = new TreeSet<>(users.keySet());
		for (Long user : totalUsers) {
			List<Tuple3<Long, Double, Long>> lst = users.get(user);
			users.put(user,
					new ArrayList<>(lst.stream().filter(t -> (items.get(t.v1) != null)).collect(Collectors.toList())));
		}

		//
		totalItems = new TreeSet<>(items.keySet());
		for (Long item : totalItems) {
			List<Tuple3<Long, Double, Long>> lst = items.get(item);
			items.put(item,
					new ArrayList<>(lst.stream().filter(t -> (users.get(t.v1) != null)).collect(Collectors.toList())));
		}

		System.out.println("Iteration: " + users.size() + " " + items.size());
	}

	/**
	 * Method to check if the k-Core fulfills the specifications
	 * 
	 * @param users
	 *            the users and their corresponding preferences
	 * @param items
	 *            the items and their corresponding preferences
	 * @param minimumRatingUsers
	 *            the minimum number of ratings required per every user
	 * @param minimumRatingItems
	 *            the minimum number of ratings required per every item
	 * @return true if the condition is satisfied, false otherwise
	 */
	private static boolean checkStateCore(Map<Long, List<Tuple3<Long, Double, Long>>> users,
			Map<Long, List<Tuple3<Long, Double, Long>>> items, int minimumRatingUsers, int minimumRatingItems) {
		for (Long user : users.keySet()) {
			if (users.get(user).size() < minimumRatingUsers) {
				return false;
			}
		}

		for (Long item : items.keySet()) {
			if (items.get(item).size() < minimumRatingItems) {
				return false;
			}
		}
		return true;
	}
	
	/***
	 * Method to parse a file generated by the MyMedialite framework
	 * 
	 * @param sourceFile
	 *            the source file
	 * @param dataModelTest
	 *            the datamodel in the test set (just to process the users that are
	 *            in the test set)
	 * @param destFile
	 *            the destination file
	 */
	public static void parseMyMediaLite(String sourceFile, DataModelIF<Long, Long> dataModelTest, String destFile) {
		BufferedReader br;
		PrintStream writer = null;
		try {

			br = new BufferedReader(new FileReader(sourceFile));
			String line = br.readLine();
			writer = new PrintStream(destFile);

			while (line != null) {
				if (line != null) {
					String[] fullData = line.split("\t");
					Long user = Long.parseLong(fullData[0]);
					if (dataModelTest.getUsers().get(user) != null) {
						line = fullData[1].replace("[", "");
						line = line.replace("]", "");
						String[] data = line.split(",");
						for (String itemAndPref : data) {
							Long item = Long.parseLong(itemAndPref.split(":")[0]);
							Double preference = Double.parseDouble(itemAndPref.split(":")[1]);
							writer.println(user + "\t" + item + "\t" + preference);
						}
					}

					line = br.readLine();
				}
			}
			br.close();
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method to obtain a new dataset transforming both the items and the users to
	 * Longs
	 * 
	 * @param srcPath
	 *            the source path
	 * @param dstPath
	 *            the destination path
	 * @param characterSplit
	 *            the character split
	 */
	public static void datasetTransformation(String srcPath, String dstPath, String characterSplit) {

		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(dstPath));
			Map<Long, Map<Long, List<Tuple2D<Double, Long>>>> storationUsers = retrieveMapTransform(srcPath,
					characterSplit);
			// print the new ratings
			for (Long user : storationUsers.keySet()) {

				for (Long item : storationUsers.get(user).keySet()) {
					for (Tuple2D<Double, Long> t : storationUsers.get(user).get(item)) {
						writer.write(user + "\t" + item + "\t" + t.item1 + "\t" + t.item2 + "\n");
					}

				}
			}

			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/***
	 * Method to retrieve the transformation of the users and the items (there can
	 * be repetitions)
	 * 
	 * @param srcPath
	 *            the source f
	 * @param characterSplit
	 *            the character split from the source file
	 * @return
	 */
	private static Map<Long, Map<Long, List<Tuple2D<Double, Long>>>> retrieveMapTransform(String srcPath,
			String characterSplit) {
		BufferedReader br;
		Map<String, Long> element1s = new TreeMap<String, Long>();
		Map<String, Long> element2s = new TreeMap<String, Long>();

		// Id user, id item rating and timestamp
		// The data to store is a map of users, with a map of items and a list
		// associated with the user and the item
		Map<Long, Map<Long, List<Tuple2D<Double, Long>>>> storationUsers = new TreeMap<Long, Map<Long, List<Tuple2D<Double, Long>>>>();

		// Variables to customize
		boolean ignoreFirstLine = false; // If we want to ignore first line, switch to true
		boolean removeDuplicates = true;
		int columnFirstElement = 0;
		int columnSecondElement = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;
		int count = 0;

		try {
			br = new BufferedReader(new FileReader(srcPath));
			Long totalElements1 = 1L;
			Long totalElements2 = 1L;
			String line = br.readLine();
			if (ignoreFirstLine)
				line = br.readLine();

			while (line != null) {
				count++;
				if (count % 100000 == 0) {
					System.out.println("100000");
				}
				if (line != null) {
					String[] data = line.split(characterSplit);
					// Transforming the new ids
					if (element1s.get(data[columnFirstElement]) == null) {
						element1s.put(data[columnFirstElement], totalElements1);
						totalElements1++;
					}
					if (element2s.get(data[columnSecondElement]) == null) {
						element2s.put(data[columnSecondElement], totalElements2);
						totalElements2++;
					}

					Long idUserTransformed = element1s.get(data[columnFirstElement]);
					Long idItemTransformed = element2s.get(data[columnSecondElement]);
					double rating = Double.parseDouble((data[columnRating]));
					Long timestamp = Long.parseLong(data[columnTimeStamp]);
					// With the new ids, we will put the the ratings to be printed in the dst file

					if (storationUsers.get(idUserTransformed) == null) { // New user
						Map<Long, List<Tuple2D<Double, Long>>> map = new TreeMap<Long, List<Tuple2D<Double, Long>>>();
						List<Tuple2D<Double, Long>> lst = new ArrayList<Tuple2D<Double, Long>>();
						lst.add(new Tuple2D<Double, Long>(rating, timestamp));
						map.put(idItemTransformed, lst);
						// Add item to list
						storationUsers.put(idUserTransformed, map);
					} else { // User exits
						Map<Long, List<Tuple2D<Double, Long>>> map = storationUsers.get(idUserTransformed);
						List<Tuple2D<Double, Long>> lst = map.get(idItemTransformed);
						if (lst == null) { // New item for the user
							lst = new ArrayList<Tuple2D<Double, Long>>();
							lst.add(new Tuple2D<Double, Long>(rating, timestamp));
							map.put(idItemTransformed, lst);
						} else {
							System.out.println("Repetitions!");
							if (removeDuplicates) {
								if (lst.get(0).item2 < timestamp) { // We obtain the newest timestamp
									lst.clear();
									lst.add(new Tuple2D<Double, Long>(rating, timestamp));
								}
							} else {
								lst.add(new Tuple2D<Double, Long>(rating, timestamp));
							}
						}
					}

				}

				line = br.readLine();
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return storationUsers;

	}
	

	/***
	 * Method that will split a full dataset in training and test depending on the
	 * training percentage. The n percent of the oldest ratings will go to train,
	 * the rest to test
	 * 
	 * @param srcPath
	 *            source path
	 * @param dstPathTrain
	 *            the destination train file
	 * @param dstPathTest
	 *            the destination test file
	 * @param trainPercent
	 *            the percentage of ratings that will go to train
	 */
	public static void DatasetTemporalGlobalSplit(String srcPath, String dstPathTrain, String dstPathTest,
			double trainPercent) {

		List<Preference> allRatings = new ArrayList<>();
		String characterSplit = "\t";

		// Parameters to configure
		int columnUser = 0;
		int columnItem = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;
		boolean ignoreFirstLine = false; // If we want to ignore first line, switch to true

		Stream<String> stream = null;
		try {
			if (ignoreFirstLine) {
				stream = Files.lines(Paths.get(srcPath)).skip(1);
			} else {
				stream = Files.lines(Paths.get(srcPath));
			}
			stream.forEach(line -> {

				String[] data = line.split(characterSplit);
				Long idUser = Long.parseLong(data[columnUser]);
				Long idItem = Long.parseLong(data[columnItem]);
				Double rating = Double.parseDouble((data[columnRating]));
				Long timeStamp = Long.parseLong(data[columnTimeStamp]);
				Preference p = new Preference(idUser, idItem, rating, timeStamp);
				allRatings.add(p);

			});
			stream.close();

			PrintStream writer = null;
			PrintStream writer2 = null;

			writer = new PrintStream(dstPathTrain);
			writer2 = new PrintStream(dstPathTest);
			// Now temporal split
			Collections.sort(allRatings);
			int indexLimit = (int) (trainPercent * allRatings.size());

			for (int i = 0; i < allRatings.size(); i++) {
				Preference p = allRatings.get(i);
				if (i < indexLimit) {
					writer.println(
							p.getIdUser() + "\t" + p.getIdItem() + "\t" + p.getRating() + "\t" + p.getTimeStamp());
				}
				else {
					writer2.println(
							p.getIdUser() + "\t" + p.getIdItem() + "\t" + p.getRating() + "\t" + p.getTimeStamp());
				}
			}

			writer2.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/***
	 * Method to order a file first by user and then by timestamp
	 * @param srcPath
	 * @param dstPathDest
	 */
	public static void DatasetOrderRatingsPerUser(String srcPath, String dstPathDest) {

		List<Preference> allRatings = new ArrayList<>();
		String characterSplit = "\t";

		// Parameters to configure
		int columnUser = 0;
		int columnItem = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;
		boolean ignoreFirstLine = false; // If we want to ignore first line, switch to true

		Stream<String> stream = null;
		try {
			if (ignoreFirstLine) {
				stream = Files.lines(Paths.get(srcPath)).skip(1);
			} else {
				stream = Files.lines(Paths.get(srcPath));
			}
			stream.forEach(line -> {

				String[] data = line.split(characterSplit);
				Long idUser = Long.parseLong(data[columnUser]);
				Long idItem = Long.parseLong(data[columnItem]);
				Double rating = Double.parseDouble((data[columnRating]));
				Long timeStamp = Long.parseLong(data[columnTimeStamp]);
				Preference p = new Preference(idUser, idItem, rating, timeStamp);
				allRatings.add(p);

			});
			stream.close();

			PrintStream writer = null;

			writer = new PrintStream(dstPathDest);
			// Now temporal split
			Collections.sort(allRatings, new PreferenceByUserComparator());

			for (int i = 0; i < allRatings.size(); i++) {
				Preference p = allRatings.get(i);
					writer.println(
							p.getIdUser() + "\t" + p.getIdItem() + "\t" + p.getRating() + "\t" + p.getTimeStamp());
			}

			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	static class Preference implements Comparable<Preference> {

		Long idUser;
		Long idItem;
		Double rating;
		Long timeStamp;

		public Preference(Long idUser, Long idItem, Double rating, Long timeStamp) {
			super();
			this.idUser = idUser;
			this.idItem = idItem;
			this.rating = rating;
			this.timeStamp = timeStamp;
		}

		public Long getIdUser() {
			return idUser;
		}

		public void setIdUser(Long idUser) {
			this.idUser = idUser;
		}

		public Long getIdItem() {
			return idItem;
		}

		public void setIdItem(Long idItem) {
			this.idItem = idItem;
		}

		public Double getRating() {
			return rating;
		}

		public void setRating(Double rating) {
			this.rating = rating;
		}

		public Long getTimeStamp() {
			return timeStamp;
		}

		public void setTimeStamp(Long timeStamp) {
			this.timeStamp = timeStamp;
		}

		@Override
		public int compareTo(Preference arg0) {
			return this.getTimeStamp().compareTo(arg0.getTimeStamp());
		}

	}
	
	/***
	 * Method that will split a dataset
	 * 
	 * @param srcpath
	 *            the source file (complete dataset)
	 * @param dstPathTrain
	 *            the destination of training interactions
	 * @param dstPathTest
	 *            the destination of test interactions
	 * @param numberOfTest
	 *            minimum number of interactions that need to go to test
	 * @param minimumNumberTrain
	 *            minimum number of interactions that need to go to train
	 */
	public static void TimeSplitPerUser(String srcpath, String dstPathTrain, String dstPathTest, int numberOfTest,
			int minimumNumberTrain) {
		// Assume structure of the file is: UserdId(long) ItemID(long) rating(double)
		// timestamp(long)

		Map<Long, ArrayList<Tuple3<Long, Double, Long>>> element1s = new TreeMap<Long, ArrayList<Tuple3<Long, Double, Long>>>();
		String characterSplit = "\t";

		// Parameters to configure
		int columnUser = 0;
		int columnItem = 1;
		int columnRating = 2;
		int columnTimeStamp = 3;

		BufferedWriter writer = null;
		BufferedWriter writer2 = null;

		Stream<String> stream = null;
		try {
			stream = Files.lines(Paths.get(srcpath));
			writer = new BufferedWriter(new FileWriter(dstPathTrain));
			writer2 = new BufferedWriter(new FileWriter(dstPathTest));

			stream.forEach(line -> {
				String[] data = line.split(characterSplit);
				Long idUser = Long.parseLong(data[columnUser]);
				if (element1s.get(idUser) == null) { // New user
					ArrayList<Tuple3<Long, Double, Long>> lst = new ArrayList<Tuple3<Long, Double, Long>>();
					Long idItem = Long.parseLong(data[columnItem]);
					double rating = Double.parseDouble((data[columnRating]));
					Long timestamp = Long.parseLong(data[columnTimeStamp]);
					lst.add(new Tuple3<Long, Double, Long>(idItem, rating, timestamp));
					// Add item to list
					element1s.put(idUser, lst);
				} else {
					ArrayList<Tuple3<Long, Double, Long>> lst = element1s.get(idUser);
					Long idItem = Long.parseLong(data[columnItem]);
					double rating = Double.parseDouble((data[columnRating]));
					Long timestamp = Long.parseLong(data[columnTimeStamp]);
					// Add item to list
					lst.add(new Tuple3<Long, Double, Long>(idItem, rating, timestamp));
				}
			});
			// Now temporal split

			for (Long user : element1s.keySet()) {
				ArrayList<Tuple3<Long, Double, Long>> lst = element1s.get(user);
				// Order data
				Collections.sort(lst, new TimeStampComparator());

				// Number of elements in test must be lower than the total size - numberOfTest.
				// If not, then go to train
				if (minimumNumberTrain <= lst.size() - numberOfTest) {
					// Store test items
					for (int i = 0; i < numberOfTest; i++) {
						Tuple3<Long, Double, Long> t = lst.remove(lst.size() - 1);
						writer2.write(user + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\n");
					}
				}
				// Rest of the list, to train
				for (Tuple3<Long, Double, Long> t : lst)
					writer.write(user + "\t" + t.v1 + "\t" + t.v2 + "\t" + t.v3 + "\n");

			}
			writer2.close();
			writer.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
}

class PreferenceByUserComparator implements Comparator<Preference>{

	@Override
	public int compare(Preference o1, Preference o2) {
		int a = o1.idUser.compareTo(o2.idUser);
		if (a == 0) {
			return o1.timeStamp.compareTo(o2.timeStamp);
		}
		return a;
	}
	
}

class TimeStampComparator implements Comparator<Tuple3<Long, Double, Long>> {
	@Override
	public int compare(Tuple3<Long, Double, Long> o1, Tuple3<Long, Double, Long> o2) {
		if (o1.v3 < o2.v3)
			return -1;
		// Deterministic
		else if (o1.v3 == o2.v3) {
			if (o1.v1 < o2.v1)
				return -1;
			else
				return 1;
		} else
			return 1;
	}



}
