/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.utils;

/**
 * Tuples with 3 items
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * 
 * @param <T1>
 *            type of first parameter
 * @param <T2>
 *            type of second parameter
 * @param <T3>
 *            type of third parameter
 */
public class Tuple3D<T1, T2, T3> {

	public T1 item1;
	public T2 item2;
	public T3 item3;

	public Tuple3D(final T1 item_1, final T2 item_2, final T3 item_3) {
		item1 = item_1;
		item2 = item_2;
		item3 = item_3;
	}

	public int size() {
		return 3;
	}
}
