/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.mains;

import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.core.util.tuples.Tuple2od;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.bfsequential.core.DataModelHetRec;
import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.recommenders.UBKNNBackwardForwardRFRecommender;
import es.uam.eps.ir.bfsequential.sim.SymmetricSimilarityBean;
import es.uam.eps.ir.bfsequential.sim.UBSimilarity;
import es.uam.eps.ir.bfsequential.sim.UBSimilarityRankSys;
import es.uam.eps.ir.bfsequential.sim.lcs.LCSUserSimilarity;
import es.uam.eps.ir.bfsequential.sim.lcs.LCSSimilarity.LCSNormalization;
import es.uam.eps.ir.bfsequential.utils.RecommendationUtils;
import es.uam.eps.ir.bfsequential.utils.Tuple3D;
import es.uam.eps.ir.bfsequential.utils.AverageRecommendationMetricIgnoreNaNs;
import es.uam.eps.ir.bfsequential.utils.LCSParser.UBTrainModel;
import es.uam.eps.ir.ranksys.core.Recommendation;

import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;

import es.uam.eps.ir.ranksys.diversity.sales.metrics.AggregateDiversityMetric;
import es.uam.eps.ir.ranksys.diversity.sales.metrics.GiniIndex;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AveragePrecision;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.ReciprocalRank;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.nn.user.UserNeighborhoodRecommender;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.TopKUserNeighborhood;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;

import es.uam.eps.ir.ranksys.novelty.longtail.FDItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.PCItemNovelty;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EFD;
import es.uam.eps.ir.ranksys.novelty.longtail.metrics.EPC;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.FreshnessMetricNorm;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.MetricScheme;
import es.uam.eps.ir.ranksys.novelty.temporal.TimestampCalculator;
import es.uam.eps.ir.ranksys.novelty.temporal.metrics.GenericFreshness;
import es.uam.eps.ir.ranksys.rec.Recommender;

/**
 * * Main experiment to splits datasets and generate recommenders. Depending on
 * the parameters this main can work in different ways.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * @author Alejandro Bellogin (alegrandro.bellogin@uam.es)
 *
 */
public class Experiment {
	// All options of the arguments
	// Option of the case
	private static final String OPT_CASE = "option";

	// Train and test files
	private static final String OPT_TRAINFILE = "trainFile";
	private static final String OPT_TESTFILE = "testFile";

	// Number of items to recommend in the recommenders
	private static final String OPT_ITEMSRECOMMENDED = "itemsRecommended";

	// Use the user and item indexes of both train and test
	private static final String OPT_COMPLETEINDEXES = "completeIndexes";

	// Similarity file path if we want to read similarities instead of calculating
	// them.
	private static final String OPT_SIMFILE = "similarityFile";

	private static final String OPT_NEIGH = "neighbours";
	private static final String OPT_OUTRESULTFILE = "outResultfile";
	private static final String OPT_OUTSIMFILE = "outSimilarityfile";

	// RankSys similarities and recommender
	private static final String OPT_RANKSYS_SIM = "ranksysSimilarity";
	private static final String OPT_RANKSYS_REC = "ranksysRecommender";

	// Saving feature information
	private static final String OPT_FEATUREREADINGFILE = "featureFile";

	// Overwrite output file or not. Default value is false.
	private static final String OPT_OVERWRITE = "outResultFileOverwrite";

	// Variables only used in rankSysEvaluation
	private static final String OPT_RECOMMENDEDFILE = "recommendedFile";
	private static final String OPT_CUTOFF = "ranksysCutoff";

	// Matching threshold to consider when are 2 ratings equal
	private static final String OPT_MATCHINGTHRESHOLD = "matching threshold";

	// Value to consider a neighbour only if similarity > confidence
	private static final String OPT_CONFIDENCE = "confidencesimilarity";

	// Indexes of special recommender
	private static final String OPT_INDEXBACKWARDS = "index backwards";
	private static final String OPT_INDEXFORWARDS = "index forwards";

	// Parameters for Ranksys Recommender MatrixFactorization
	private static final String OPT_KFACTORIZER = "k factorizer value";
	private static final String OPT_ALPHAFACTORIZER = "alpha factorizer value";
	private static final String OPT_LAMBDAFACTORIZER = "lambda factorizer value";
	private static final String OPT_NUMINTERACTIONSFACTORIZER = "num interactions factorizer value";

	// File that will store the indexes between users (indexes of the last item that
	// have been rated by both users)
	private static final String OPT_TEMPORALLAMBDA = "temporal lambda";

	// Parameters of Aggregation library
	private static final String OPT_NORMAGGREGATELIBRARY = "norm of aggregate library";
	private static final String OPT_COMBAGGREGATELIBRARY = "comb of aggregate library";
	private static final String OPT_WEIGHTAGGREGATELIBRARY = "weight of aggregate library";
	private static final String OPT_IDENTIFICATIONFACTOR = "identification factor";
	private static final String OPT_RATINGFACTOR = "rating factor";
	private static final String OPT_LCSTRAINMODEL = "lcs train model";
	private static final String OPT_LCSNORMALIZATION = "lcs normalization";
	private static final String OPT_BESTINDEXES = "obtain best indexes";

	private static final String OPT_TIMESAVERAGEITEMS = "file of average time items";
	private static final String OPT_TIMESFIRSTITEMS = "file of first time items";
	private static final String OPT_TIMESLASTITEMS = "file of last time items";
	private static final String OPT_TIMESMEDIANITEMS = "file of median time items";

	public static void main(String[] args) throws Exception {
		String step = "";
		CommandLine cl = getCommandLine(args);
		if (cl == null) {
			System.out.println("Error in arguments");
			return;
		}

		// Obtain the arguments these 2 are obligatory
		step = cl.getOptionValue(OPT_CASE);
		String trainFile = cl.getOptionValue(OPT_TRAINFILE);

		switch (step) {

		case "lcs4recsysSaveUBSimFile": {
			/*
			 * Necessary arguments: -Option of the case. -Training file. -outputSimFile. The
			 * file where the similarities will be stored -Actors file. -Directors file.
			 * -Genres file. -LCS train model (operate with actors, directors or genres)
			 * -LCS normalization (returning just the LCS or returning LCS*LCS/u1.lenght*
			 * u2*lenght) -threshold the difference between ratings to consider them equals
			 * 
			 */
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUTSIMFILE);

			String lcsTrainModel = cl.getOptionValue(OPT_LCSTRAINMODEL);
			String lcsNormalization = cl.getOptionValue(OPT_LCSNORMALIZATION);
			String factorIDs = cl.getOptionValue(OPT_IDENTIFICATIONFACTOR);
			String factorRating = cl.getOptionValue(OPT_RATINGFACTOR);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			String confidence = cl.getOptionValue(OPT_CONFIDENCE);
			Double confidenceD;

			int idFactor = 100;
			int ratingFactor = 1;
			if (confidence == null)
				confidenceD = Double.NaN;
			else
				confidenceD = Double.parseDouble(confidence);

			// Only if not null.
			if (factorIDs != null && factorRating != null) {
				idFactor = Integer.parseInt(factorIDs);
				ratingFactor = Integer.parseInt(factorRating);
			}

			int threshold = Integer.parseInt(cl.getOptionValue(OPT_MATCHINGTHRESHOLD, "0"));

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");
			File f = new File(outputSimFile);

			// If the result file exist and overwrite is to false, finish the
			// program
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			DataModelIF<Long, Long> lcs4DataModel = new DataModelHetRec(true, false);
			lcs4DataModel.loadFileTrain(trainFile);
			LCSUserSimilarity simlcs4 = null;
			if (testFile != null) {
				DataModelIF<Long, Long> lcs4DataModelTest = new DataModelHetRec(true, false);
				lcs4DataModelTest.loadFileTrain(testFile);
				// Create the similarity with train model, with threshold and with
				// lcsNormalization
				simlcs4 = new LCSUserSimilarity(lcs4DataModel, lcs4DataModelTest, obUBTrainModel(lcsTrainModel),
						threshold, obtLCSNormalization(lcsNormalization), idFactor, ratingFactor);
			} else {
				simlcs4 = new LCSUserSimilarity(lcs4DataModel, lcs4DataModel, obUBTrainModel(lcsTrainModel), threshold,
						obtLCSNormalization(lcsNormalization), idFactor, ratingFactor);
			}
			simlcs4.save(outputSimFile, confidenceD);

		}
			break;

		// Reading no normalization file -> obtain another normalization file
		case "lcs4recsysCreateNormFile": {
			/*
			 * Necessary arguments: -Option of the case. -Training file. -Sim file
			 * -outputSimFile. -LCS train model -LCS normalization This arguments only if
			 * needed: -Genres file -Actors file -Directors file -Tags file
			 * 
			 * 
			 */
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUTSIMFILE);
			String lcsTrainModel = cl.getOptionValue(OPT_LCSTRAINMODEL);
			String lcsNormalization = cl.getOptionValue(OPT_LCSNORMALIZATION);
			String simFile = cl.getOptionValue(OPT_SIMFILE);

			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");

			String factorIDs = cl.getOptionValue(OPT_IDENTIFICATIONFACTOR);
			String factorRating = cl.getOptionValue(OPT_RATINGFACTOR);

			int idFactor = 100;
			int ratingFactor = 1;

			// Only if not null.
			if (factorIDs != null && factorRating != null) {
				idFactor = Integer.parseInt(factorIDs);
				ratingFactor = Integer.parseInt(factorRating);
			}

			DataModelIF<Long, Long> lcs4DataModel = new DataModelHetRec(true, false);
			lcs4DataModel.loadFileTrain(trainFile);

			File f = new File(outputSimFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			// Threshold is necessary for gap normalization
			UBTrainModel modelTrain = obUBTrainModel(lcsTrainModel);

			LCSUserSimilarity simlcs4 = null;
			simlcs4 = new LCSUserSimilarity(lcs4DataModel, modelTrain, obtLCSNormalization(lcsNormalization), simFile,
					idFactor, ratingFactor);

			simlcs4.save(outputSimFile, Double.NaN);
		}
			break;

		case "ranksysSaveUBSimFile": {
			System.out.println(Arrays.toString(args));
			String outputSimFile = cl.getOptionValue(OPT_OUTSIMFILE);
			String ransksysSim = cl.getOptionValue(OPT_RANKSYS_SIM);
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "true");
			Boolean completeIndexes = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, "false"));
			File f = new File(outputSimFile);

			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			final Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = retrieveTrainingTestIndexes(
					completeIndexes, trainFile, trainFile, lp, lp);
			final List<Long> usersTraining = indexes.v1;
			final List<Long> itemsTraining = indexes.v2;

			FastUserIndex<Long> userIndex = SimpleFastUserIndex.load(usersTraining.stream());
			FastItemIndex<Long> itemIndex = SimpleFastItemIndex.load(itemsTraining.stream());
			FastPreferenceData<Long, Long> ranksysTrainData = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndex, itemIndex);

			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simRankSys = RecommendationUtils
					.obtRanksysUserSimilarity(ranksysTrainData, ransksysSim);
			Map<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>> similarities = new HashMap<SymmetricSimilarityBean<Long>, Tuple3D<Double, Integer, Integer>>();

			// For all users
			simRankSys.getAllUsers().forEach(u1 -> {
				// Similar to the user
				simRankSys.similarUsers(u1).forEach(sim -> {
					similarities.put(new SymmetricSimilarityBean<Long>(u1, sim.v1),
							new Tuple3D<Double, Integer, Integer>(sim.v2, -1, -1));
				});
			});

			UBSimilarity recUBSim = new UBSimilarity(similarities);
			recUBSim.save(outputSimFile, Double.NaN);
		}
			break;

		case "ranksysTolc4recsysLoadingUBSimFile":
		case "ranksysTolc4recsysLoadingSimFile": {
			/*
			 * Necessary arguments: -Option of the case. -Training file. -OutputFile -Users
			 * file. -Items file. -testFile -Similarity file to load -numberItemsRecommend
			 * in the recommendation -neighbours to use in the recommendations
			 * 
			 */
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String simFile = cl.getOptionValue(OPT_SIMFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));
			Boolean completeIndexes = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, "false"));

			System.out.println(Arrays.toString(args));
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");
			File f = new File(outputFile);
			// If the result file exist and overwrite is to false, finish the
			// program
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			final Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = retrieveTrainingTestIndexes(
					completeIndexes, trainFile, testFile, lp, lp);
			final List<Long> usersTraining = indexes.v1;
			final List<Long> itemsTraining = indexes.v2;

			final List<Long> usersTest = indexes.v3;
			final List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTraining.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTraining.stream());

			FastUserIndex<Long> usersIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemsIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

			FastPreferenceData<Long, Long> ranksysTrainData = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);
			FastPreferenceData<Long, Long> ranksystestData = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), usersIndexTest, itemsIndexTest);

			// First sim file will be the similarity used in the rankSysNeighbourhood
			es.uam.eps.ir.ranksys.nn.user.sim.UserSimilarity<Long> simRankSys = new UBSimilarityRankSys(
					ranksysTrainData, simFile, Double.NaN);

			UserNeighborhood<Long> rankSysNeighborhood = new TopKUserNeighborhood<>(simRankSys, neighbours);

			Recommender<Long, Long> rankSysrecaauxiliary = null;

			// Normal RankSys Recommender
			rankSysrecaauxiliary = new UserNeighborhoodRecommender<Long, Long>(
					ranksysTrainData, rankSysNeighborhood, 1);

			// Now we print all the recommended items of the rankSysRec
			RecommendationUtils.ranksysWriteRanking(ranksysTrainData, ranksystestData, rankSysrecaauxiliary, outputFile,
					numberItemsRecommend);

		}
			break;

		// BackWardUB Recommender Map aggregator
		case "recSysBackwardMapAggregator": {
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String simFile = cl.getOptionValue(OPT_SIMFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");
			String indexBackwards = cl.getOptionValue(OPT_INDEXBACKWARDS);
			String indexForwards = cl.getOptionValue(OPT_INDEXFORWARDS);
			int indexBackwardsParsed = Integer.parseInt(indexBackwards);
			int indexForwardsParsed = Integer.parseInt(indexForwards);

			String combiner = cl.getOptionValue(OPT_COMBAGGREGATELIBRARY);
			String normalizer = cl.getOptionValue(OPT_NORMAGGREGATELIBRARY);
			String weightaux = cl.getOptionValue(OPT_WEIGHTAGGREGATELIBRARY, "false");
			boolean weight = Boolean.parseBoolean(weightaux);
			boolean obtainBestIndexes = Boolean.parseBoolean(cl.getOptionValue(OPT_BESTINDEXES, "false"));
			System.out.println("Weight is " + weight);

			// If the result file exist and overwrite is to false, finish the
			// program
			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			DataModelIF<Long, Long> dataModel = new DataModelHetRec(true, false);
			dataModel.loadFileTrain(trainFile);

			UBSimilarity ubSim = new UBSimilarity(simFile);
			UBKNNBackwardForwardRFRecommender rec = new UBKNNBackwardForwardRFRecommender(dataModel, ubSim, neighbours,
					neighbours, indexBackwardsParsed, indexForwardsParsed, normalizer, combiner, weight);
			rec.setNeighbours(neighbours);
			rec.train();

			DataModelIF<Long, Long> dataModelTest = new DataModelHetRec(true, false);
			dataModelTest.loadFileTrain(testFile);

			if (obtainBestIndexes)
				rec.setObtainBestIndexes(true);
			else
				rec.setObtainBestIndexes(false);

			// For each user of the second dataset
			rec.writeRanking(outputFile, numberItemsRecommend, dataModelTest);

		}
			break;

		// Ranksys only (it operates only with ranksys framework, it does not
		// transform to our dataModel)
		case "ranksysOnlyComplete": {
			System.out.println(Integer.MAX_VALUE);
			/*
			 * Necessary arguments: -Option of the case. -Training file. -OutputFile -Users
			 * file. -Items file. -testFile -ranksysimilarity the string of the similarity
			 * that ranksys framework will use -ranksysRecommender the string of the
			 * recommeder that ranksys framework will use -numberItemsRecommend in the
			 * recommendation -neighbours to use in the recommendations
			 * 
			 */
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			Boolean completeIndexes = Boolean.parseBoolean(cl.getOptionValue(OPT_COMPLETEINDEXES, "false"));

			String ranksysSimilarity = cl.getOptionValue(OPT_RANKSYS_SIM);

			String ranksysRecommender = cl.getOptionValue(OPT_RANKSYS_REC);
			String testFile = cl.getOptionValue(OPT_TESTFILE);

			// Check if we include factorizers
			String kFactorizer = cl.getOptionValue(OPT_KFACTORIZER);
			String alphaFactorizer = cl.getOptionValue(OPT_ALPHAFACTORIZER);
			String lambdaFactorizer = cl.getOptionValue(OPT_LAMBDAFACTORIZER);
			String numInteractions = cl.getOptionValue(OPT_NUMINTERACTIONSFACTORIZER);
			String temporalLambdaS = cl.getOptionValue(OPT_TEMPORALLAMBDA);
			String simFile = cl.getOptionValue(OPT_SIMFILE);

			int kFactorizeri = 50;
			double alphaFactorizeri = 1;
			double lambdaFactorizeri = 0.1;
			int numInteractioni = 20;

			if (ranksysSimilarity == null) {
				System.out.println("Need at least one similarity in ranksys");
				return;
			}


			if (kFactorizer != null) {
				kFactorizeri = Integer.parseInt(kFactorizer);
				System.out.println("Working with kFactorizeri=" + kFactorizeri);
			}

			if (alphaFactorizer != null) {
				alphaFactorizeri = Double.parseDouble(alphaFactorizer);
				System.out.println("Working with alphaFactorizer=" + alphaFactorizeri);
			}
			if (lambdaFactorizer != null) {
				lambdaFactorizeri = Double.parseDouble(lambdaFactorizer);
				System.out.println("Working with lambdaFactorizer=" + lambdaFactorizer);
			}
			if (numInteractions != null) {
				numInteractioni = Integer.parseInt(numInteractions);
				System.out.println("Working with numInteractions=" + numInteractions);
			}

			double temporalLambda = 1.0 / 200.0;
			if (temporalLambdaS != null)
				temporalLambda = Double.parseDouble(temporalLambdaS);

			final int numberItemsRecommend = Integer.parseInt(cl.getOptionValue(OPT_ITEMSRECOMMENDED));
			int neighbours = Integer.parseInt(cl.getOptionValue(OPT_NEIGH));

			System.out.println(Arrays.toString(args));
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");
			File f = new File(outputFile);
			// If the result file exist and overwrite is to false, finish the
			// program
			if (f.exists() && !f.isDirectory() && Boolean.parseBoolean(overwrite) == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			final Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = retrieveTrainingTestIndexes(
					completeIndexes, trainFile, testFile, lp, lp);
			final List<Long> usersTraining = indexes.v1;
			final List<Long> itemsTraining = indexes.v2;

			final List<Long> usersTest = indexes.v3;
			final List<Long> itemsTest = indexes.v4;

			FastUserIndex<Long> userIndexTrain = SimpleFastUserIndex.load(usersTraining.stream());
			FastItemIndex<Long> itemIndexTrain = SimpleFastItemIndex.load(itemsTraining.stream());
			FastPreferenceData<Long, Long> ranksysTrainData = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp), userIndexTrain, itemIndexTrain);

			FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
			FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());
			FastPreferenceData<Long, Long> ranksystestData = SimpleFastPreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

			DataModelIF<Long, Long> lcs4RecsysDM = new DataModelHetRec();

			// Four the temporal recommender we need to load our DataModel as it has
			// timestamps
			if (ranksysRecommender.contains("RankSysTemporalRecommender")) {
				lcs4RecsysDM.loadFileTrain(trainFile);
			}

			Recommender<Long, Long> rankSysrec = RecommendationUtils.obtRankSysRecommeder(ranksysRecommender,
					ranksysSimilarity, ranksysTrainData, neighbours, userIndexTrain, itemIndexTrain,
					kFactorizeri, alphaFactorizeri, lambdaFactorizeri, numInteractioni, lcs4RecsysDM, temporalLambda, simFile);

			RecommendationUtils.ranksysWriteRanking(ranksysTrainData, ranksystestData, rankSysrec, outputFile,
					numberItemsRecommend);

		}
			break;

		case "DatasetTransform": {
			System.out.println("-o DatasetTransform -trf completeData transformed");
			RecommendationUtils.datasetTransformation(trainFile, args[4], args[5]);
		}
			break;

		case "DatasetReduction": {
			System.out.println("-o DatasetReduction -trf srcpath dstPath numberOfRatingsUser numberOfRatingsItems");
			System.out.println(Arrays.toString(args));
			RecommendationUtils.DatasetReductionRatings(trainFile, args[4], Integer.parseInt(args[5]),
					Integer.parseInt(args[6]));
		}
			break;

		case "TemporalSplitPerUser": {
			System.out.println("-o TemporalSplitPerUser -trf data trainDest testDest numberOfTest numberOfTrain");
			RecommendationUtils.TimeSplitPerUser(trainFile, args[4], args[5], Integer.parseInt(args[6]),
					Integer.parseInt(args[7]));
		}
			break;

		case "AverageSetOfResults": {
			System.out.println(Arrays.toString(args));
			RecommendationUtils.averageResults(trainFile, args[4], args[5]);
		}
			break;

		case "TemporalGlobalSplit": {
			System.out.println("-o TemporalGlobalSplit -trf completeFile dstPathTrain dstPathTest trainPercent");
			System.out.println(Arrays.toString(args));
			RecommendationUtils.DatasetTemporalGlobalSplit(trainFile, args[4], args[5], Double.parseDouble(args[6]));
		}
			break;
			
		case "OrderByUsersTimestamp": {
			System.out.println("-o OrderByUsersTimestamp -trf completeFile dstPathTest");
			System.out.println(Arrays.toString(args));
			RecommendationUtils.DatasetOrderRatingsPerUser(trainFile, args[4]);
		}
		break;	

		case "ParseMyMediaLite": {
			System.out.println("-o ParseMyMediaLite -trf myMediaLiteRecommendation testfile newRecommendation");
			System.out.println(Arrays.toString(args));

			DataModelIF<Long, Long> dataModelTest = new DataModelHetRec();

			dataModelTest.loadFileTrain(args[4]);

			RecommendationUtils.parseMyMediaLite(trainFile, dataModelTest, args[5]);
		}
			break;

		// Ranksys with non accuracy metrics
		case "ranksysNonAccuracyWithoutFeatureMetricsEvaluation":
		case "ranksysNonAccuracyMetricsEvaluation": {
			/*
			 * -Train file -Test file -Recommended file -Item feature file -Ranksys Metric
			 * -Output file -Threshold -Cutoff
			 */
			System.out.println(Arrays.toString(args));
			String outputFile = cl.getOptionValue(OPT_OUTRESULTFILE);
			String testFile = cl.getOptionValue(OPT_TESTFILE);
			String recommendedFile = cl.getOptionValue(OPT_RECOMMENDEDFILE);

			int threshold = Integer.parseInt(cl.getOptionValue(OPT_MATCHINGTHRESHOLD));
			int cutoff = Integer.parseInt(cl.getOptionValue(OPT_CUTOFF));
			String overwrite = cl.getOptionValue(OPT_OVERWRITE, "false");

			String itemsTimeAvgFile = cl.getOptionValue(OPT_TIMESAVERAGEITEMS);
			String itemsTimeFirstFile = cl.getOptionValue(OPT_TIMESFIRSTITEMS);
			String itemsTimeMedianFile = cl.getOptionValue(OPT_TIMESMEDIANITEMS);
			String itemsTimeLastFile = cl.getOptionValue(OPT_TIMESLASTITEMS);

			Boolean overwriteB = Boolean.parseBoolean(overwrite);
			File f = new File(outputFile);
			if (f.exists() && !f.isDirectory() && overwriteB == false) {
				System.out.println("Ignoring " + f + " because it already exists");
				return;
			}

			PreferenceData<Long, Long> trainData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(trainFile, lp, lp));
			PreferenceData<Long, Long> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp));
			PreferenceData<Long, Long> totalData = new ConcatPreferenceData<>(trainData, testData);

			PreferenceData<Long, Long> recommendedData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(recommendedFile, lp, lp));

			// BINARY RELEVANCE
			BinaryRelevanceModel<Long, Long> binRel = new BinaryRelevanceModel<>(false, testData, threshold);

			RelevanceModel<Long, Long> selectedRelevance = new NoRelevanceModel<Long, Long>();

			int numUsersTest = testData.numUsersWithPreferences();
			int numUsersRecommended = 0;
			numUsersRecommended = recommendedData.numUsersWithPreferences();

			System.out.println("\n\nNum users to average ranksys non-accuracy " + numUsersTest);
			System.out.println("\n\nNum users to average ranksys ranking " + numUsersRecommended);

			Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();

			////////////////////////
			// INDIVIDUAL METRICS //
			////////////////////////

			DataModelHetRec dataModel = new DataModelHetRec(true, false);
			dataModel.loadFileTrain(trainFile);
			double maxTime = RecommendationUtils.getLargestTimeStamp(dataModel);
			double minTime = RecommendationUtils.getMinimumTimeStamp(dataModel);
			RankingDiscountModel discModel = new NoDiscountModel();

			System.out.println("Max time " + maxTime);
			System.out.println("Min time " + minTime);

			TimestampCalculator<Long, Long> its = new TimestampCalculator<>();
			Map<MetricScheme, String> mapSchemeFile = new HashMap<>();

			if (itemsTimeAvgFile != null && itemsTimeFirstFile != null && itemsTimeMedianFile != null
					&& itemsTimeLastFile != null) {
				File fAvg = new File(itemsTimeAvgFile);
				File ffirst = new File(itemsTimeFirstFile);
				File fmed = new File(itemsTimeMedianFile);
				File flast = new File(itemsTimeLastFile);
				mapSchemeFile.put(MetricScheme.FIRST, itemsTimeFirstFile);
				mapSchemeFile.put(MetricScheme.LAST, itemsTimeLastFile);
				mapSchemeFile.put(MetricScheme.AVG, itemsTimeAvgFile);
				mapSchemeFile.put(MetricScheme.MEDIAN, itemsTimeMedianFile);

				if (!fAvg.exists() || !ffirst.exists() || !fmed.exists() || !flast.exists()) {
					its.computeInteractionTimes(trainFile, lp, "\t", 1, 3);
					its.save(mapSchemeFile);
				} else {
					its.load(mapSchemeFile, lp);
				}
			}

			DataModelHetRec dataModelTest = new DataModelHetRec(true, false);
			dataModelTest.loadFileTrain(testFile);

			// Ranking metrics (avg for recommendation)
			Map<String, RecommendationMetric<Long, Long>> recMetrics = new HashMap<>();
			recMetrics.put("Precision_" + threshold,
					new es.uam.eps.ir.ranksys.metrics.basic.Precision<>(cutoff, binRel));
			recMetrics.put("MAP_" + threshold, new AveragePrecision<>(cutoff, binRel));
			recMetrics.put("Recall_" + threshold, new es.uam.eps.ir.ranksys.metrics.basic.Recall<>(cutoff, binRel));
			recMetrics.put("MRR_" + threshold, new ReciprocalRank<>(cutoff, binRel));
			recMetrics.put("NDCG_" + threshold,
					new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testData, threshold)));
			recMetrics.put("epc", new EPC<>(cutoff, new PCItemNovelty<>(trainData), selectedRelevance, discModel));
			recMetrics.put("efd", new EFD<>(cutoff, new FDItemNovelty<>(trainData), selectedRelevance, discModel));

			// If we have files of first, last median and average of the timestamps of each
			// item
			if (itemsTimeAvgFile != null && itemsTimeFirstFile != null && itemsTimeMedianFile != null
					&& itemsTimeLastFile != null) {
				recMetrics.put("adi",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.NO_NORM, MetricScheme.AVG),
								selectedRelevance, discModel));
				recMetrics.put("fdi",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.NO_NORM, MetricScheme.FIRST),
								selectedRelevance, discModel));
				recMetrics.put("mdi",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.NO_NORM, MetricScheme.MEDIAN),
								selectedRelevance, discModel));
				recMetrics.put("ldi",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.NO_NORM, MetricScheme.LAST),
								selectedRelevance, discModel));

				// Freshness metrics: ADI, FDI, MDI and LDI (MinMaxNormalization)
				recMetrics.put("adiMinMaxNorm",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.AVG),
								selectedRelevance, discModel));
				recMetrics.put("fdMinMaxNorm",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.FIRST),
								selectedRelevance, discModel));
				recMetrics.put("mdiMinMaxNorm", new GenericFreshness<>(cutoff,
						new ItemFreshness<>(trainData, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.MEDIAN),
						selectedRelevance, discModel));
				recMetrics.put("ldiMinMaxNorm",
						new GenericFreshness<>(cutoff,
								new ItemFreshness<>(trainData, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.LAST),
								selectedRelevance, discModel));
			}

			int finalUsersRec = numUsersRecommended;
			// Average of all
			recMetrics.forEach((name, metric) -> sysMetrics.put(name + "_test",
					new AverageRecommendationMetricIgnoreNaNs<>(metric, numUsersTest)));
			recMetrics.forEach((name, metric) -> sysMetrics.put(name + "_rec",
					new AverageRecommendationMetricIgnoreNaNs<>(metric, finalUsersRec)));

			////////////////////
			// SYSTEM METRICS //
			////////////////////
			sysMetrics.put("aggrdiv", new AggregateDiversityMetric<>(cutoff, selectedRelevance));
			int numItems = totalData.numItemsWithPreferences();
			sysMetrics.put("gini", new GiniIndex<>(cutoff, numItems));
			sysMetrics.put("usercov", new SystemMetric<Long, Long>() {
				private Set<Long> users = new TreeSet<>();

				@Override
				public void add(Recommendation<Long, Long> arg0) {
					users.add(arg0.getUser());
				}

				@Override
				public void combine(SystemMetric<Long, Long> arg0) {
				}

				@Override
				public double evaluate() {
					return 1.0 * users.size();
				}

				@Override
				public void reset() {
					users.clear();
				}
			});

			sysMetrics.put("usercov_rel", new SystemMetric<Long, Long>() {
				private Set<Long> users = new TreeSet<>();

				@Override
				public void add(Recommendation<Long, Long> arg0) {
					if (binRel.getModel(arg0.getUser()).getRelevantItems().size() > 0)
						users.add(arg0.getUser());
				}

				@Override
				public void combine(SystemMetric<Long, Long> arg0) {
				}

				@Override
				public double evaluate() {
					return 1.0 * users.size();
				}

				@Override
				public void reset() {
					users.clear();
				}
			});

			sysMetrics.put("RealAD", new SystemMetric<Long, Long>() {
				private Set<Long> items = new TreeSet<>();

				@Override
				public void add(Recommendation<Long, Long> arg0) {
					Set<Long> newItems = arg0.getItems().stream().map(Tuple2od<Long>::v1).collect(Collectors.toSet());
					items.addAll(newItems);
				}

				@Override
				public void combine(SystemMetric<Long, Long> arg0) {
				}

				@Override
				public double evaluate() {
					return 1.0 * items.size();
				}

				@Override
				public void reset() {
					items.clear();
				}
			});

			RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);

			format.getReader(recommendedFile).readAll()
					.forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));

			PrintStream out = new PrintStream(new File(outputFile));
			sysMetrics.forEach((name, metric) -> out.println(name + "\t" + metric.evaluate()));
			out.close();

		}
			break;

		default:
			System.out.println("Option " + step + " not recognized");
			break;
		}
	}

	private static LCSNormalization obtLCSNormalization(String norm) {
		if (norm.equals("No") || norm.equals("NoNormalization") || norm.equals("NONORMALIZATION"))
			return LCSNormalization.NONORMALIZATION;
		else if (norm.equals("MAX") || norm.equals("LCSDivMax") || norm.equals("LCSDIVMAX"))
			return LCSNormalization.LCSDIVMAX;
		else if (norm.equals("MIN") || norm.equals("LCSDivMin") || norm.equals("LCSDIVMIN"))
			return LCSNormalization.LCSDIVMIN;
		else if (norm.equals("POW2DIVMULT") || norm.equals("LCSDivMULT") || norm.equals("LCSPOW2DIVMULT"))
			return LCSNormalization.LCSPOW2DIVMULT;
		else if (norm.equals("DOUBLEDIVSUM") || norm.equals("LCSPlusPlusDivSum") || norm.equals("LCSPLUSLCSDIVSUM"))
			return LCSNormalization.LCSPLUSLCSDIVSUM;
		else
			return LCSNormalization.NONORMALIZATION;
	}

	private static UBTrainModel obUBTrainModel(String train) {
		for (UBTrainModel m : UBTrainModel.values()) {
			if (m.toString().equals(train)) {
				return m;
			}
		}
		return null;
	}

	/**
	 * Method that will obtain a command line using the available options of the
	 * arguments
	 *
	 * @param args
	 *            the arguments that the program will receive
	 * @return a command line if the arguments are correct or null if an error
	 *         occurred
	 */
	private static CommandLine getCommandLine(String[] args) {
		Options options = new Options();

		// Number of the case
		Option caseIdentifier = new Option("o", OPT_CASE, true, "option of the case");
		caseIdentifier.setRequired(true);
		options.addOption(caseIdentifier);

		// Train file
		Option trainFile = new Option("trf", OPT_TRAINFILE, true, "input file train path");
		trainFile.setRequired(true);
		options.addOption(trainFile);

		// TestFile file
		Option testFile = new Option("tsf", OPT_TESTFILE, true, "input file test path");
		testFile.setRequired(false);
		options.addOption(testFile);

		// Similarity file
		Option similarityFile = new Option("sf", OPT_SIMFILE, true, "similarity file");
		similarityFile.setRequired(false);
		options.addOption(similarityFile);

		// Neighbours
		Option neighbours = new Option("n", OPT_NEIGH, true, "neighbours");
		neighbours.setRequired(false);
		options.addOption(neighbours);

		// ThresholdSimilarity
		Option thresholdSim = new Option("thr", OPT_MATCHINGTHRESHOLD, true, "Matching threshold");
		thresholdSim.setRequired(false);
		options.addOption(thresholdSim);

		// NumberItemsRecommended
		Option numberItemsRecommended = new Option("nI", OPT_ITEMSRECOMMENDED, true, "Number of items recommended");
		numberItemsRecommended.setRequired(false);
		options.addOption(numberItemsRecommended);

		// OutResultfile
		Option outfile = new Option("orf", OPT_OUTRESULTFILE, true, "output result file");
		outfile.setRequired(false);
		options.addOption(outfile);

		// OutSimilarityfile
		Option outSimfile = new Option("osf", OPT_OUTSIMFILE, true, "output similarity file");
		outSimfile.setRequired(false);
		options.addOption(outSimfile);

		// Ranksys similarity
		Option rankSysSim = new Option("rs", OPT_RANKSYS_SIM, true, "ranksys similarity");
		rankSysSim.setRequired(false);
		options.addOption(rankSysSim);

		// Ranksys recommender
		Option rankSysRecommender = new Option("rr", OPT_RANKSYS_REC, true, "ranksys recommeder");
		rankSysRecommender.setRequired(false);
		options.addOption(rankSysRecommender);

		// Overwrite result
		Option outputOverwrite = new Option("ovw", OPT_OVERWRITE, true, "overwrite");
		outputOverwrite.setRequired(false);
		options.addOption(outputOverwrite);

		// Feature reading file
		Option featureReadingFile = new Option("ff", OPT_FEATUREREADINGFILE, true, "features file");
		featureReadingFile.setRequired(false);
		options.addOption(featureReadingFile);

		// Feature reading file
		Option recommendedFile = new Option("rf", OPT_RECOMMENDEDFILE, true, "recommended file");
		recommendedFile.setRequired(false);
		options.addOption(recommendedFile);

		// RankSysMetric
		Option ranksysCutoff = new Option("rc", OPT_CUTOFF, true, "ranksyscutoff");
		ranksysCutoff.setRequired(false);
		options.addOption(ranksysCutoff);

		// Confidence similarity (if similarity is lower than this value, the similarity
		// will consider )
		Option confidenceSimilarity = new Option("cs", OPT_CONFIDENCE, true, "confidence similarity");
		confidenceSimilarity.setRequired(false);
		options.addOption(confidenceSimilarity);

		// IndexBackWards
		Option indexBackward = new Option("indexb", OPT_INDEXBACKWARDS, true, "index backwards");
		indexBackward.setRequired(false);
		options.addOption(indexBackward);

		// IndexForwards
		Option indexForward = new Option("indexf", OPT_INDEXFORWARDS, true, "index forwards");
		indexForward.setRequired(false);
		options.addOption(indexForward);

		// RankSys factorizers (k)
		Option kFactorizer = new Option("kFactorizer", OPT_KFACTORIZER, true, "k factorizer");
		kFactorizer.setRequired(false);
		options.addOption(kFactorizer);

		// RankSys factorizers (alpha)
		Option alhpaFactorizer = new Option("aFactorizer", OPT_ALPHAFACTORIZER, true, "alpha factorizer");
		alhpaFactorizer.setRequired(false);
		options.addOption(alhpaFactorizer);

		// RankSys factorizers (lambda)
		Option lambdaFactorizer = new Option("lFactorizer", OPT_LAMBDAFACTORIZER, true, "lambda factorizer");
		lambdaFactorizer.setRequired(false);
		options.addOption(lambdaFactorizer);

		// RankSys factorizers (numInteractions)
		Option numInteractionsFact = new Option("nIFactorizer", OPT_NUMINTERACTIONSFACTORIZER, true,
				"numInteractions factorizer");
		numInteractionsFact.setRequired(false);
		options.addOption(numInteractionsFact);

		// Lambda parameter for ranksys Time recommender
		Option temporalLambda = new Option("tempLambda", OPT_TEMPORALLAMBDA, true, "temporal lambda");
		temporalLambda.setRequired(false);
		options.addOption(temporalLambda);

		// RankLibrary norm value
		Option normAggregate = new Option("normAgLib", OPT_NORMAGGREGATELIBRARY, true,
				"normalization aggregate library");
		normAggregate.setRequired(false);
		options.addOption(normAggregate);

		// RankLibrary comb value
		Option combAggregate = new Option("combAgLib", OPT_COMBAGGREGATELIBRARY, true, "combination aggregate library");
		combAggregate.setRequired(false);
		options.addOption(combAggregate);

		// RankLibrary norm value
		Option weightAggregate = new Option("weightAgLib", OPT_WEIGHTAGGREGATELIBRARY, true,
				"weight aggregate library");
		weightAggregate.setRequired(false);
		options.addOption(weightAggregate);

		// Identification factor
		Option identificatorFactor = new Option("idLCSf", OPT_IDENTIFICATIONFACTOR, true, "identification factor");
		identificatorFactor.setRequired(false);
		options.addOption(identificatorFactor);

		// Identification factor
		Option ratingFactor = new Option("rLCSf", OPT_RATINGFACTOR, true, "rating factor");
		ratingFactor.setRequired(false);
		options.addOption(ratingFactor);

		// TrainModel
		Option lcsTrainModel = new Option("lcstm", OPT_LCSTRAINMODEL, true, "lcs training model");
		lcsTrainModel.setRequired(false);
		options.addOption(lcsTrainModel);

		// LCS normalization: true or false
		Option lcsNormalization = new Option("lcsn", OPT_LCSNORMALIZATION, true, "lcs normalization");
		lcsNormalization.setRequired(false);
		options.addOption(lcsNormalization);

		Option obtainBestIndexess = new Option("bestInd", OPT_BESTINDEXES, true, "obtain best indexes");
		obtainBestIndexess.setRequired(false);
		options.addOption(obtainBestIndexess);

		// Complete Indexes
		Option completeIndexes = new Option("cI", OPT_COMPLETEINDEXES, true, "complete indexes");
		completeIndexes.setRequired(false);
		options.addOption(completeIndexes);

		Option itemsTimeAverage = new Option("ItimeAvgFile", OPT_TIMESAVERAGEITEMS, true, "average item time file");
		itemsTimeAverage.setRequired(false);
		options.addOption(itemsTimeAverage);

		Option itemsTimeFirst = new Option("ItimeFstFile", OPT_TIMESFIRSTITEMS, true, "first item time file");
		itemsTimeFirst.setRequired(false);
		options.addOption(itemsTimeFirst);

		Option itemsTimeLast = new Option("ItimeLastFile", OPT_TIMESLASTITEMS, true, "last item time file");
		itemsTimeLast.setRequired(false);
		options.addOption(itemsTimeLast);

		Option itemsTimeMedian = new Option("ItimeMedFile", OPT_TIMESMEDIANITEMS, true,
				"median base for discount model");
		itemsTimeMedian.setRequired(false);
		options.addOption(itemsTimeMedian);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			return null;
		}
		return cmd;

	}

	/**
	 *
	 * Method that returns the users and items lists for training and test splits.
	 *
	 * @param <U>
	 *            type of users
	 * @param <I>
	 *            type of items
	 * @param completeIndexes
	 *            flag to denote if an aggregated index should be created for
	 *            training and test, or if separate indexes should be returned
	 * @param trainingFile
	 *            file with the training file
	 * @param testFile
	 *            file with the test set
	 * @param up
	 *            parser of users
	 * @param ip
	 *            parser of items
	 * @return a tuple of users and items from training, and users and items from
	 *         test
	 */
	private static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainingTestIndexes(
			boolean completeIndexes, String trainingFile, String testFile, Parser<U> up, Parser<I> ip) {
		List<U> usersTraining = null;
		List<I> itemsTraining = null;
		List<U> usersTest = null;
		List<I> itemsTest = null;
		if (completeIndexes) {
			Tuple2<List<U>, List<I>> userItems = getCompleteUserItems(trainingFile, testFile, up, ip);
			usersTraining = userItems.v1;
			itemsTraining = userItems.v2;
			usersTest = userItems.v1;
			itemsTest = userItems.v2;
		} else {
			Tuple2<List<U>, List<I>> userItemsTrain = getUserItemsFromFile(trainingFile, up, ip);
			usersTraining = userItemsTrain.v1;
			itemsTraining = userItemsTrain.v2;
			Tuple2<List<U>, List<I>> userItemsTest = getUserItemsFromFile(testFile, up, ip);
			usersTest = userItemsTest.v1;
			itemsTest = userItemsTest.v2;
		}
		return new Tuple4<>(usersTraining, itemsTraining, usersTest, itemsTest);
	}

	/**
	 *
	 * Method to obtain the user and the items from the complete training and test
	 * data
	 *
	 * @param <U>
	 *            type of users
	 * @param <I>
	 *            type of items
	 * @param fileTraining
	 *            the training file
	 * @param fileTest
	 *            the test file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return a tuple with the list of users and items
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTraining, String fileTest,
			Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> trainingData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTraining, up, ip));
			PreferenceData<U, I> testData = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
			PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainingData, testData);
			List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 *
	 * Method to obtain a tuple of users and items from a given file
	 *
	 * @param <U>
	 *            type of users
	 * @param <I>
	 *            type of items
	 * @param file
	 *            the file
	 * @param up
	 *            the user parser
	 * @param ip
	 *            the item parser
	 * @return a tuple with the list of users and items
	 */
	public static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
		try {
			PreferenceData<U, I> data = SimplePreferenceData
					.load(SimpleRatingPreferencesReader.get().read(file, up, ip));
			List<U> usersList = data.getAllUsers().collect(Collectors.toList());
			List<I> itemsList = data.getAllItems().collect(Collectors.toList());

			return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
					itemsList.stream().sorted().collect(Collectors.toList()));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
