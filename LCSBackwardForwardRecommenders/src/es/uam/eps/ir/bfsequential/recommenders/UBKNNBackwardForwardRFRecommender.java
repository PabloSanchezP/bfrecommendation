/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.recommenders;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.core.GenericUser;
import es.uam.eps.ir.bfsequential.core.Neighbour;
import es.uam.eps.ir.bfsequential.core.RecommenderIF;
import es.uam.eps.ir.bfsequential.core.GenericUser.PredictedItemPreference;
import es.uam.eps.ir.bfsequential.sim.UBSimilarity;
import es.uam.eps.ir.bfsequential.utils.RecommendationUtils;
import es.uam.eps.ir.bfsequential.utils.Tuple2D;
import es.uam.eps.ir.bfsequential.utils.Tuple3D;
import es.uam.eps.nets.rankfusion.GenericRankAggregator;
import es.uam.eps.nets.rankfusion.GenericResource;
import es.uam.eps.nets.rankfusion.GenericSearchResults;
import es.uam.eps.nets.rankfusion.combination.AnzCombiner;
import es.uam.eps.nets.rankfusion.combination.MNZCombiner;
import es.uam.eps.nets.rankfusion.combination.MaxCombiner;
import es.uam.eps.nets.rankfusion.combination.MedCombiner;
import es.uam.eps.nets.rankfusion.combination.MinCombiner;
import es.uam.eps.nets.rankfusion.combination.SumCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFCombiner;
import es.uam.eps.nets.rankfusion.interfaces.IFNormalizer;
import es.uam.eps.nets.rankfusion.interfaces.IFRankAggregator;
import es.uam.eps.nets.rankfusion.interfaces.IFResource;
import es.uam.eps.nets.rankfusion.interfaces.IFSearchResults;
import es.uam.eps.nets.rankfusion.normalization.DistributionNormalizer;
import es.uam.eps.nets.rankfusion.normalization.DummyNormalizer;
import es.uam.eps.nets.rankfusion.normalization.RankSimNormalizer;
import es.uam.eps.nets.rankfusion.normalization.StdNormalizer;
import es.uam.eps.nets.rankfusion.normalization.SumNormalizer;
import es.uam.eps.nets.rankfusion.normalization.TwoMUVNormalizer;
import es.uam.eps.nets.rankfusion.normalization.ZMUVNormalizer;
import es.uam.eps.nets.util.math.Function.Distribution;

/**
 * Recommender that recommends items by moving along neighbours sequences using
 * the indexes of the last items that they have rated.
 *
 * When we have all items, we aggregate them in a map and order them by a
 * ranking score
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 * @author Alejandro Bellogin (alegrandro.bellogin@uam.es)
 *
 */
public class UBKNNBackwardForwardRFRecommender implements RecommenderIF<Long, Long> {

	protected final static int DEFAULT_BACKWARDS = 3;
	protected final static int DEFAULT_FORWARDS = 3;

	protected DataModelIF<Long, Long> dataModel;
	protected int k; // k used in test
	protected int kTrain; // k used in train
	protected UBSimilarity similarity;

	protected int indexBackWards = DEFAULT_BACKWARDS;
	protected int indexForwards = DEFAULT_FORWARDS;

	protected boolean obtainBestIndexes = false;

	private boolean weightNeighboursBySimilarity;

	private IFRankAggregator rankAggregator;

	public UBKNNBackwardForwardRFRecommender(DataModelIF<Long, Long> dataModel, UBSimilarity sim, int k, int kTrain,
			int indexbackwards, int indexforwards, String normalizer, String combiner,
			boolean weightNeighboursBySimilarity) {
		if (!dataModel.isUserBased()) {
			throw new IllegalArgumentException("Datamodel is not prepared for users!");
		}
		this.dataModel = dataModel;
		this.k = k;
		this.similarity = sim;
		int maxK = dataModel.getUsers().size();
		this.kTrain = (kTrain > maxK ? maxK : kTrain);
		this.indexBackWards = indexbackwards;
		this.indexForwards = indexforwards;

		this.weightNeighboursBySimilarity = weightNeighboursBySimilarity;

		IFNormalizer norm = getNormalizer(normalizer, null, null);
		IFCombiner comb = getCombiner(combiner);
		rankAggregator = new GenericRankAggregator(norm, comb);
	}

	//
	public UBKNNBackwardForwardRFRecommender(DataModelIF<Long, Long> dataModel, UBSimilarity sim, int k, int kTrain) {
		this(dataModel, sim, kTrain, kTrain, DEFAULT_BACKWARDS, DEFAULT_FORWARDS, "defaultnorm", "defaultcomb", true);
	}

	@Override
	public List<PredictedItemPreference<Long>> recommend(Long user, int numberItems) {
		List<PredictedItemPreference<Long>> items = new ArrayList<>();
		// check if user was seen in training
		if (!dataModel.getUsers().containsKey(user)) {
			return items;
		}

		/*
		 * Method to recommend -> Obtain lists of recommendations. with a value of
		 * similarity Aggregate that lists
		 */
		GenericUser<Long, Long> gU = dataModel.getUsers().get(user);
		// List<Tuple2D<Double, List<Tuple2D<Long, Double>>>> lst = new
		// ArrayList<Tuple2D<Double, List<Tuple2D<Long, Double>>>>();
		List<Long> timeStampOrderedU1 = new ArrayList<>(RecommendationUtils
				.sortByValue(this.dataModel.getUsers().get(user).getItemsTimeStamp(), false).keySet());

		List<IFSearchResults> neighbourList = new ArrayList<>();

		// System.out.println("recommendation for " + user);
		for (Neighbour<Long> n : gU.getNeighbours()) {
			Long neigh = n.getId();
			Tuple3D<Double, Integer, Integer> t3d = similarity.completeSimilarity(user, neigh);

			List<Long> timeStampOrdered = new ArrayList<>(RecommendationUtils
					.sortByValue(this.dataModel.getUsers().get(neigh).getItemsTimeStamp(), false).keySet());

			// check indices
			if (obtainBestIndexes || ((t3d.item2 == -1) || (t3d.item3 == -1))) {
				Tuple2D<Integer, Integer> t = null;
				t = RecommendationUtils.obtainBestIndexesLCSEquivalent(timeStampOrderedU1, timeStampOrdered);

				// If the indexes were -1 then update them
				if (((t3d.item2 == -1) || (t3d.item3 == -1))) {
					if (user.compareTo(neigh) < 0) {
						t3d.item2 = t.item1;
						t3d.item3 = t.item2;
					} else {
						t3d.item2 = t.item2;
						t3d.item3 = t.item1;
					}
				}
			}
			Tuple3D<Double, Integer, Integer> copy = new Tuple3D<Double, Integer, Integer>(t3d.item1, t3d.item2,
					t3d.item3);

			double sim = copy.item1;

			List<Long> iF = null;
			List<Long> iB = null;
			if (user.compareTo(neigh) < 0) {
				iF = userForward(gU.getId(), n.getId(), copy.item3, sim, timeStampOrdered);
				iB = userBackwards(gU.getId(), n.getId(), copy.item3, sim, timeStampOrdered);
			} else {
				iF = userForward(gU.getId(), n.getId(), copy.item2, sim, timeStampOrdered);
				iB = userBackwards(gU.getId(), n.getId(), copy.item2, sim, timeStampOrdered);
			}
			Map<Long, IFResource> neighbourRanking = new HashMap<>();

			int counter = 0;
			int rank = 0;
			while (true) {
				if (iF.size() > counter) {
					String idItem = iF.get(counter).toString();
					neighbourRanking.put(new Long(idItem.hashCode()), generateScore(idItem, rank,
							dataModel.getUsers().get(neigh).getItemsRated().get(iF.get(counter)), sim));
					rank++;
				}

				if (iB.size() > counter) {
					String idItem = iB.get(counter).toString();
					neighbourRanking.put(new Long(idItem.hashCode()), generateScore(idItem, rank,
							dataModel.getUsers().get(neigh).getItemsRated().get(iB.get(counter)), sim));
					rank++;
				}
				counter++;

				if (iF.size() <= counter && iB.size() <= counter) {
					break;
				}
			}

			double neighbourWeight = (weightNeighboursBySimilarity ? sim : 1.0);
			IFSearchResults neighbourResults = new GenericSearchResults(neighbourRanking, neighbourWeight);
			neighbourResults.setNotRetrievedNormalizedValue(0.0);
			neighbourList.add(neighbourResults);
		}

		// Get sorted aggregated list
		if (rankAggregator != null && neighbourList != null && neighbourList.size() != 0) {
			List<IFResource> aggResults = rankAggregator.aggregate(neighbourList)
					.getSortedResourceList(GenericSearchResults.I_RESOURCE_ORDER_COMBINED_VALUE);
			for (IFResource aggResult : aggResults) {
				items.add(
						new PredictedItemPreference<>(Long.parseLong(aggResult.getId()), aggResult.getCombinedValue()));
			}
		}

		// It should be ordered
		Collections.sort(items, Collections.reverseOrder());
		// Avoid IndexOutOfBoundException
		return items.subList(0, Math.min(numberItems, items.size()));

	}

	private List<Long> userBackwards(Long u1, Long neigh, int index, double sim, List<Long> timeStampOrdered) {
		List<Long> result = new ArrayList<>();
		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move backwards, the index indicated the last index that
		 * matched with this user 3 - Index must be greater than zero so we don't obtain
		 * a negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexBackWards && ((timeStampOrdered.size() - 1) >= (index - 1))
				&& ((index - 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index - 1);
			// if u1 has not rated it
			if (this.dataModel.getUsers().get(u1).getItemsRated().get(idItem) == null) {
				result.add(idItem);
			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index--;
		}
		return result;
	}

	private List<Long> userForward(Long u1, Long neigh, int index, double sim, List<Long> timeStampOrdered) {
		List<Long> result = new ArrayList<>();

		/*
		 * 1 - Limit has the counter of maximum items that can be recommended by that
		 * user 2 - As we move forward, the index indicated the last index that matched
		 * with this user 3 - Index must be greater than zero so we don't obtain a
		 * negative index. Moreover, index must be lower than the list of users size
		 */
		for (int i = 0; i < indexForwards && ((timeStampOrdered.size() - 1) >= (index + 1))
				&& ((index + 1) >= 0); i++) {
			Long idItem = timeStampOrdered.get(index + 1);
			// if u1 has not rated it
			if (this.dataModel.getUsers().get(u1).getItemsRated().get(idItem) == null) {
				// result.add(new Tuple2D<Long, Double>(idItem, ratingNeigh));
				result.add(idItem);
			} else {
				// If we have a match, we must decrement to retrieve limit items
				i--;
			}
			// check the next item
			index++;
		}
		return result;
	}

	private IFResource generateScore(String id, int rankPosition, double rating, double sim) {
		return new GenericResource(id, rating, rankPosition);
	}

	@Override
	public String toString() {
		String s = "UBKNNBackwardForwardRFRecommender: " + k + "_" + similarity;
		return s;
	}

	private static IFCombiner getCombiner(String combiner) {
		IFCombiner comb = null;
		if (combiner.compareTo("sumcomb") == 0) {
			comb = new SumCombiner();
		} else if (combiner.compareTo("mincomb") == 0) {
			comb = new MinCombiner();
		} else if (combiner.compareTo("mnzcomb") == 0) {
			comb = new MNZCombiner();
		} else if (combiner.compareTo("anzcomb") == 0) {
			comb = new AnzCombiner();
		} else if (combiner.compareTo("maxcomb") == 0) {
			comb = new MaxCombiner();
		} else if (combiner.compareTo("medcomb") == 0) {
			comb = new MedCombiner();
		} else if (combiner.compareTo("defaultcomb") == 0) {
			// Default combiner
			comb = new SumCombiner();
		} else {
			// Unrecognized combiner
			comb = null;
		}
		return comb;
	}

	private static IFNormalizer getNormalizer(String normalizer, Distribution idealDist, Distribution dataDist) {
		IFNormalizer norm = null;
		if (normalizer.compareTo("stdnorm") == 0) {
			norm = new StdNormalizer();
		} else if (normalizer.compareTo("zmuvnorm") == 0) {
			norm = new ZMUVNormalizer();
		} else if (normalizer.compareTo("ranksimnorm") == 0) {
			norm = new RankSimNormalizer();
		} else if (normalizer.compareTo("sumnorm") == 0) {
			norm = new SumNormalizer();
		} else if (normalizer.compareTo("2muvnorm") == 0) {
			norm = new TwoMUVNormalizer();
			// norm = new StdNormalizer();
		} else if (normalizer.compareTo("distrnorm") == 0) {
			Map<String, Distribution> mapDist = new HashMap<>();
			mapDist.put("", dataDist);
			norm = new DistributionNormalizer(idealDist, mapDist);
		} else if (normalizer.compareTo("defaultnorm") == 0) {
			// Default normalizer
			norm = new DummyNormalizer();
		} else {
			// Unrecognized normalizer
			norm = null;
		}
		return norm;
	}

	@Override
	public void train() {
		for (Long user : dataModel.getUsers().keySet()) {
			dataModel.getUsers().get(user).setMaxNeighbours(kTrain);
			for (Long user2 : dataModel.getUsers().keySet()) {
				if (user.equals(user2)) // If they are the same
				{
					continue;
				}

				Double sim = similarity.getSimilarity(user, user2);
				if (sim != null && !Double.isNaN(sim)) {
					dataModel.getUsers().get(user).addNeighbour(user2, sim);
				}
			}
		}
	}

	@Override
	public double predictInteraction(Long user, Long item) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setNeighbours(int neighbours) {
		this.k = neighbours;
	}

	public boolean isObtainBestIndexes() {
		return obtainBestIndexes;
	}

	public void setObtainBestIndexes(boolean obtainBestIndexes) {
		this.obtainBestIndexes = obtainBestIndexes;
	}

	@Override
	public void writeRanking(String path, int numberItems, DataModelIF<Long, Long> dataModel) {
		PrintStream out;
		try {
			out = new PrintStream(path);
			int c = 0;
			for (Long user : dataModel.getUsers().keySet()) {
				// We recommend
				// Obtain the itemsRecommended
				List<PredictedItemPreference<Long>> recs = recommend(user, numberItems);
				if (recs.isEmpty()) {
					c++;
				}
				int rank = 1;
				// Put the ranks
				for (PredictedItemPreference<Long> ri : recs) {
					out.println(RecommendationUtils.formatRank(user, ri.getIdItem(), ri.getPreference(), rank));
					rank++;
				}
			}
			System.out.println("Empty recommendations for " + c + " users out of " + dataModel.getUsers().size());
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
