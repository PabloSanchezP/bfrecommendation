/*******************************************************************************
 * Copyright (C) 2018 Pablo Sánchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.bfsequential.recommenders;

import java.util.ArrayList;

import es.uam.eps.ir.bfsequential.core.DataModelIF;
import es.uam.eps.ir.bfsequential.utils.RecommendationUtils;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.nn.user.neighborhood.UserNeighborhood;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/**
 *
 * Implementation of a time decay recommender in RankSys as defined in p.32 of:
 * Time-aware recommender systems: a comprehensive survey and analysis of
 * existing evaluation protocols P.G. Campos, F. Diez, I. Cantador. UMUAI 2014.
 *
 * @author Pablo Sanchez (pablo.sanchezp@uam.es)
 *
 * @param <U> the type of users
 * @param <I> the type of items
 */
public class RankSysTemporalRecommender<U extends Comparable<U>, I extends Comparable<I>>
		extends FastRankingRecommender<U, I> {

	// Implementation of recommender (page 32):
	// http://download.springer.com/static/pdf/713/art%253A10.1007%252Fs11257-012-9136-x.pdf?originUrl=http%3A%2F%2Flink.springer.com%2Farticle%2F10.1007%2Fs11257-012-9136-x&token2=exp=1488969447~acl=%2Fstatic%2Fpdf%2F713%2Fart%25253A10.1007%25252Fs11257-012-9136-x.pdf%3ForiginUrl%3Dhttp%253A%252F%252Flink.springer.com%252Farticle%252F10.1007%252Fs11257-012-9136-x*~hmac=00114f68c62a8b02e791d07e73b6427c536c563cd0b60d70af0071204321a9e6

	DataModelIF<Long, Long> lcs4RecSysDatamodel;
	double lambda = 1.0 / 200.0;
	private final double t;
	private final double timeInterval;
	private final double softenMult;

	public RankSysTemporalRecommender(FastPreferenceData<U, I> data, UserNeighborhood<U> neighborhood, int q,
			DataModelIF<Long, Long> lcs4RecSysDatamodel, double lambda) {
		// 24*3600 default value to compute the number of days and 2 the multiplier
		this(data, neighborhood, q, lcs4RecSysDatamodel, lambda, 24 * 3600, 2.0);
	}

	public RankSysTemporalRecommender(FastPreferenceData<U, I> data, UserNeighborhood<U> neighborhood, int q,
			DataModelIF<Long, Long> lcs4RecSysDatamodel, double lambda, double timeInterval, double softMultiplier) {
		super(data, data);
		this.data = data;
		this.neighborhood = neighborhood;
		this.q = q;
		this.lambda = lambda;
		this.lcs4RecSysDatamodel = lcs4RecSysDatamodel;
		t = 1.0 / lambda;
		this.timeInterval = timeInterval;
		this.softenMult = softMultiplier;
	}

	/**
	 * Preference data.
	 */
	protected final FastPreferenceData<U, I> data;

	/**
	 * User neighborhood.
	 */
	protected final UserNeighborhood<U> neighborhood;

	/**
	 * Exponent of the similarity.
	 */
	protected final int q;

	@Override
	public Int2DoubleMap getScoresMap(int uidx) {

		Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
		scoresMap.defaultReturnValue(0.0);

		// We must obtain last timestampOrdered time of user u
		Long userID = (Long) data.uidx2user(uidx);
		if (lcs4RecSysDatamodel.getUsers().get(userID) != null) {// User MUST exist in train
			Long lastItemUTrain = new ArrayList<Long>(RecommendationUtils
					.sortByValue(lcs4RecSysDatamodel.getUsers().get(userID).getItemsTimeStamp(), true).keySet()).get(0);
			Long lastTimeUTrain = lcs4RecSysDatamodel.getUsers().get(userID).getItemsTimeStamp().get(lastItemUTrain);

			neighborhood.getNeighbors(uidx).forEach(vs -> {

				int neighbourIndex = vs.v1; // ranksysIndex
				Long neighbourID = new Long((Long) data.uidx2user(neighbourIndex)); // lcs4recsysUserId
				double w = Math.pow(vs.v2, q);
				data.getUidxPreferences(vs.v1).forEach(iv -> {
					Long itemId = (Long) data.iidx2item(iv.v1);
					Long timeStampItemNeighbours = lcs4RecSysDatamodel.getUsers().get(neighbourID).getItemsTimeStamp()
							.get(itemId);

					Long diff = lastTimeUTrain - timeStampItemNeighbours;

					int ndays = (int) Math.round(diff / (this.timeInterval));

					// If the number of days is negative and the absolute is higher than the number
					// of t of the lambda * the Mult
					if (ndays < 0 && Math.abs(ndays) > (this.t * this.softenMult))
						ndays = -(int) (this.softenMult * this.t);

					double p = w * iv.v2 * Math.pow(Math.E, -lambda * (ndays));
					scoresMap.addTo(iv.v1, p);
				});

			});
		}

		return scoresMap;
	}

}
