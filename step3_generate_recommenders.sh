#!/bin/sh

# Description
: '
Recommender creation and evaluation (for validation and final evaluation)
Most of the recommenders can be generated in parallel (see symbols &). Uncomment them in order to allow parallelization.
However, take into account that every additional recommender will need an additional core
'

# Java variables
JAR=LCSBackwardForwardRecommenders/target/LCSBackwardForwardRecommenders-0.0.1-SNAPSHOT.jar
jvmMemory=-Xmx16G

# Files/paths variables
ttpathfiles=TrainTest
recommendedFolder=RecommendationFolder
mkdir -p $recommendedFolder
similaritiesFolder=Sim
resultsFolder=ResultsFolder
mkdir -p $resultsFolder
datapathfiles=TrainTest
ubsimPrefix=sim
ibsimPrefix=ibsim
recPrefix=rec
resultsPrefix=eval
naresultsPrefix=naeval

# Evaluation variables
itemsRecommended=200
allneighbours="5 10 20 30 40 50 60 70 80 90 100"
alltemporalLambdas="0.1 0.05 0.02 0.01" # Values from paper of Ding 2005
allindexesBackwardForward="2 5 10"
allRuiningHEKs="2 5 10"
allRuiningHELs="1 2 3"
allRuiningHeLambdas="0.1 0.2"
allKFactorizerRankSys="10 50 100"
allLambdaFactorizerRankSys="0.1 1 10"
allAlphaFactorizerRankSys="0.1 1 10 100"


javaCommand=java

# Nomenclature for Datasets
EpinionsName=Epinions
FoursqrName=Foursqr
MovieTweetings_600KName=MovieTweetings_600K

# Extension of Files
extensionGlobalTrain=_global.train
extensionGlobalTest=_global.test
extensionPerUserTrain=_SplitPerUser.train
extensionPerUserTest=_SplitPerUser.test
extensionValidation=Validation
extensionParsedRemoveDuplicates=extensionParsedRemoveDuplicates


# MyMediaLite variables
mymedialitePath=MyMediaLite-3.11/bin
BPRFactors=$allKFactorizerRankSys
BPRBiasReg="0 0.5 1"
BPRLearnRate=0.05
BPRNumIter=50
BPRRegU="0.0025 0.001 0.005 0.01 0.1 0.0005"
BPRRegJ="0.00025 0.0001 0.0005 0.001 0.01 0.00005"
extensionMyMediaLite=MyMedLt



NoBinRelResults=ResultRankSysValidationNoBinRelNoDisc
BinRelResults=ResultRankSysValidationBinRelNoDisc

# Caser parameters
caser_Ls="2"
caser_Ts="1 2"
caser_n_iters="30" # default is 50, but i will use 30
caser_seeds="1234"
caser_batch_sizes="512"
caser_learning_rates="0.003"
caser_l2s="0.000001"
caser_neg_samples="3"
caser_use_cuda="False"
caser_ds="10 50"
caser_nvs="4"
caser_nhs="4 16"
caser_drops="0.5"
caser_ac_convs="relu"
caser_ac_fcs="relu"
pathCaser=Caser_python_adapted


# Recommenders
for fold in "$EpinionsName"0 "$EpinionsName""$extensionValidation"0 "$MovieTweetings_600KName"0 "$MovieTweetings_600KName""$extensionValidation"0 "$FoursqrName"0 "$FoursqrName""$extensionValidation"0
do
	# Obtain users and items file depending on the fold we are in
	for splitType in PERUSER GLOBAL
	do
		trainfile=""
		testfile=""

		trainfileCaser=""
		testfileCaser=""

		if [[ "$splitType" == "GLOBAL" ]]; then
			echo "GlobalSplit"
			trainfile=$ttpathfiles/"$fold""$extensionGlobalTrain"
			testfile=$ttpathfiles/m$fold"$extensionGlobalTest"


			trainfileCaser=$ttpathfiles/ORDEREDUSER_"$fold""$extensionGlobalTrain"
			testfileCaser=$ttpathfiles/ORDEREDUSER_m$fold"$extensionGlobalTest"

			#Generate the train-test files for caser

			$javaCommand $jvmMemory -jar $JAR -o OrderByUsersTimestamp -trf $trainfile $trainfileCaser
			$javaCommand $jvmMemory -jar $JAR -o OrderByUsersTimestamp -trf $testfile $testfileCaser


		fi

		if [[ "$splitType" == "PERUSER" ]]; then
			echo "PerUserSplit"
			trainfile=$ttpathfiles/"$fold""$extensionPerUserTrain"
			testfile=$ttpathfiles/m$fold"$extensionPerUserTest"

			trainfileCaser=$trainfile
			testfileCaser=$testfile
		fi
		echo "files analyzed"
		echo "$trainfile"
		echo "$testfile"



		# Neighbours is put to 20 because this recommender does not use it (Popularity)
		outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_"Popularity".txt
		$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr "PopularityRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -cI true

		outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_"RandomRecommender"TrainItems.txt
		$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr "RandomRecommender" -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -cI true


		# CASER PYTHON
			for caser_L in $caser_Ls
			do
				for caser_T in $caser_Ts
				do
					for caser_n_iter in $caser_n_iters
					do
						for caser_batch_size in $caser_batch_sizes
						do
							for caser_learning_rate in $caser_learning_rates
							do
								for caser_l2 in $caser_l2s
								do
									for caser_neg_sample in $caser_neg_samples
									do
										for caser_d in $caser_ds
										do
											for caser_nv in $caser_nvs
											do
												for caser_nh in $caser_nhs
												do
													for caser_drop in $caser_drops
													do

														outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_Caser_L"$caser_L"_T"$caser_T"_Iter"$caser_n_iter"_BatchS"$caser_batch_size"_learnR"$caser_learning_rate"_l2"$caser_l2"_negSample"$caser_neg_sample"_d"$caser_d"_nv"$caser_nv"_nh"$caser_nh"_drop"$caser_drop"_ORDEREDUSER.txt
														if [ ! -f *"$outputRecfile"* ]; then
															python3 $pathCaser/train_caser.py --train_root $trainfileCaser --test_root $testfileCaser --L $caser_L --T $caser_T --n_iter $caser_n_iter --batch_size $caser_batch_size --learning_rate $caser_learning_rate --l2 $caser_l2 --neg_samples $caser_neg_sample --d $caser_d --nv $caser_nv --nh $caser_nh --drop $caser_drop --nItems $itemsRecommended --TrainItems True --output $outputRecfile
														fi

													done
													wait
												done # End caser nh
												wait
											done # End nvs
											wait
										done # End ds
										wait
									done # End negsamples
									wait
								done # End l2
								wait
							done # End learning rate
							wait
						done # End caser batch size
						wait
					done # End caser n iters
					wait
				done # End T caser
				wait
			done # End L caser
			wait




		# Parameters to be the same of the paper https://www.ismll.uni-hildesheim.de/mymedialite/documentation/mdoc/MyMediaLite.ItemRecommendation/BPRMF.html
		# BPR
		for repetition in 1 2 3 4 5
		do
			for factor in $BPRFactors
			do
				for bias_reg in $BPRBiasReg
				do
					for regU in $BPRRegU # Regularization for items and users is the same
					do
						regJ=$(echo "$regU/10" | bc -l)

						outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_"$extensionMyMediaLite"_BPRMF_nFact"$factor"_nIter"$BPRNumIter"_LearnR"$BPRLearnRate"_BiasR"$bias_reg"_RegU"$regU"_RegI"$regU"_RegJ"$regJ""Rep$repetition".txt
						echo $outputRecfile
						if [ ! -f *"$outputRecfile"* ]; then
							outputRecfile2=$outputRecfile"Aux".txt
							echo "./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter""

							./$mymedialitePath/item_recommendation --training-file=$trainfile --recommender=BPRMF --prediction-file=$outputRecfile2 --predict-items-number=$itemsRecommended --recommender-options="num_factors=$factor bias_reg=$bias_reg reg_u=$regU reg_i=$regU reg_j=$regJ learn_rate=$BPRLearnRate UniformUserSampling=false WithReplacement=false num_iter=$BPRNumIter"
							$javaCommand $jvmMemory -jar $JAR -o ParseMyMediaLite -trf $outputRecfile2 $testfile $outputRecfile
							rm $outputRecfile2
						fi
					done # End regU
					wait
				done # End bias_reg
				wait
			done # End factor
			wait
		done # End repetition
		wait


		# Ruining He baselines (repetitions)
		for repetition in 1 2 3 4 5
		do
			for HeK in $allRuiningHEKs
			do
				for HeLambda in $allRuiningHeLambdas
				do
					for HeL in $allRuiningHELs
					do
						for recommmender in Fossil mc fpmc
						do
							outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_"$recommmender"_K"$HeK"_Lambda"$HeLambda"_L"$HeL""Rep$repetition".txt
							echo $outputRecfile
							echo " "
							echo "./train $trainfile 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile 200 $recommmender $testfile"
							if [ ! -f *"$outputRecfile"* ]; then
								"Fossil_RankingModification/"./train $trainfile 0 0 $HeL $HeK  $HeLambda  100  50000  model_save_path  0 0 $outputRecfile 200 $recommmender $testfile
							fi
						done # End recommender
						wait
					done # End HeL
					wait
				done # End He lambda
				wait
			done # End He K
			wait
		done # End repetition
		wait


		# NeighbourhoodBased (Ranksys)
		for neighbours in $allneighbours
		do
			# Get all simFiles of that fold
			find $similaritiesFolder/ -name "$ubsimPrefix"_"$fold"_"$splitType"_* | while read simFile; do # Obtain all similarities associated to the fold
			simFileName=$(basename "$simFile" .txt) # extension removed


			outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksysReadingSimFile_UB_k"$neighbours"_"$simFileName".txt
			$javaCommand $jvmMemory -jar $JAR -o "ranksysTolc4recsysLoadingUBSimFile" -trf $trainfile -orf $outputRecfile -sf $simFile -tsf $testfile -nI $itemsRecommended -n $neighbours


			# BackwardForward recommenders
			for index in $allindexesBackwardForward
			do
				for weight in true false
				do
					for normalizer in defaultnorm stdnorm ranksimnorm
					do
						# OnlyForward
						outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_"BF"_OF"$index"_UB_k"$neighbours"_"$simFileName"_norm"$normalizer"_Weight"$weight"_Csumcomb.txt
						$javaCommand $jvmMemory -jar $JAR -o recSysBackwardMapAggregator -trf $trainfile -orf $outputRecfile -sf $simFile -tsf $testfile -nI $itemsRecommended -n $neighbours -indexb 0 -indexf $index -normAgLib $normalizer -combAgLib sumcomb -weightAgLib $weight #&

					done # End normalizer
					wait

					for normalizer in defaultnorm stdnorm ranksimnorm
					do

						# OnlyBackward
						outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_"BF"_OB"$index"_UB_k"$neighbours"_"$simFileName"_norm"$normalizer"_Weight"$weight"_Csumcomb.txt
						$javaCommand $jvmMemory -jar $JAR -o recSysBackwardMapAggregator -trf $trainfile -orf $outputRecfile -sf $simFile -tsf $testfile -nI $itemsRecommended -n $neighbours -indexb $index -indexf 0 -normAgLib $normalizer -combAgLib sumcomb -weightAgLib $weight #&

					done # End normalizer
					wait

					for normalizer in defaultnorm stdnorm ranksimnorm
					do

						# BackwardForward
						outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_"BF"_BothFB"$index"_UB_k"$neighbours"_"$simFileName"_norm"$normalizer"_Weight"$weight"_Csumcomb.txt
						$javaCommand $jvmMemory -jar $JAR -o recSysBackwardMapAggregator -trf $trainfile -orf $outputRecfile -sf $simFile -tsf $testfile -nI $itemsRecommended -n $neighbours -indexb $index -indexf $index -normAgLib $normalizer -combAgLib sumcomb -weightAgLib $weight #&
					done # End normalizer
					wait



				done # End weight
				wait

			done # End indexes
			wait

		done # End similarities
		wait

		# Ranksys
		# Ranksys with similarities UserBased
		for UBsimilarity in VectorCosineUserSimilarity VectorJaccardUserSimilarity SetJaccardUserSimilarity
		do
			outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_UB_"$UBsimilarity"_k"$neighbours".txt
			$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr "UserNeighborhoodRecommender" -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile


			# TemporalRecommender RankSys
			for temporalLambda in $alltemporalLambdas
			do
				# Temporal KNN recommender rankys
				outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_UB_RankSysTemporalRecommender_"$UBsimilarity"_"TemporalLambda""$temporalLambda"_k"$neighbours".txt
				$javaCommand $jvmMemory -jar $JAR  -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr "RankSysTemporalRecommender" -rs $UBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile -tempLambda $temporalLambda #&
			done # End temporal lambda
			wait

		done # End UB similaritye
		wait


		# Ranksys with similarities ItemBased
		for IBsimilarity in VectorCosineItemSimilarity VectorJaccardItemSimilarity SetJaccardItemSimilarity
		do
			outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_IB_"$IBsimilarity"_k"$neighbours".txt
			$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr "ItemNeighborhoodRecommender" -rs $IBsimilarity -nI $itemsRecommended -n $neighbours -orf $outputRecfile #&
		done # End IB similarity
		wait


	done # End Neighbours
	wait


	# HKV baselines (repetitions)
	for repetition in 1 2 3 4 5
	do
		for rankRecommenderNoSim in MFRecommenderHKV
		do
			for kFactor in $allKFactorizerRankSys
			do
				for lambdaValue in $allLambdaFactorizerRankSys
				do
					for alphaValue in $allAlphaFactorizerRankSys
					do
						# Neighbours is put to 20 because this recommender does not use it
						outputRecfile=$recommendedFolder/"$recPrefix"_"$fold"_"$splitType"_ranksys_"$rankRecommenderNoSim"_kFactor"$kFactor"_aFactorizer"$alphaValue"_LFactorizer""$lambdaValue"Rep$repetition".txt
						$javaCommand $jvmMemory -jar $JAR -o ranksysOnlyComplete -trf $trainfile -tsf $testfile -rr $rankRecommenderNoSim -rs "notUsed" -nI $itemsRecommended -n 20 -orf $outputRecfile -kFactorizer $kFactor -aFactorizer $alphaValue -lFactorizer $lambdaValue #&
					done # Alpha
					wait
				done # Lambda value
				wait
			done # K factor
			wait
		done # Rank recommendation
		wait
	done # repetition
	wait




done #End of split type
wait


done # End of folds
wait
